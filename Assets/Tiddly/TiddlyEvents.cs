using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TiddlyEvents
{
    public delegate void OnFlickLiftChangedDelegate(float liftValue);    // 0 to 1
    public static OnFlickLiftChangedDelegate OnFlickLiftChanged;
}
