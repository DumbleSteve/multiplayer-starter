using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


// represents a TiddlyWinks token which can be 'flicked'
// raycasts down from above to see if it is covered (squopped)
// in any part by another token

public class TiddlyToken : PlayerToken
{
    [SerializeField] private float flickForce = 4000f;
    [SerializeField] private float pushForce = 250f;                    // away from hit point, towards centre
    [SerializeField] private float flickLiftModifier = 0.2f;            // moves explosion point down (to create lift)

    private float flickLiftFactor = 0f;                                 // 0 - 1, according to oscillating UI slider

    [SerializeField] private LayerMask squopLayers;                     // for spherecast down (to see if token is covered)
    [SerializeField] private Color squoppedColour = Color.red;          // for spherecast down (to see if token is covered)
    [SerializeField] private Color cantMoveColour = Color.red;          // ! CanMove
    [SerializeField] private Color canFlickColour = Color.white;        // for spherecast down (to see if token is covered)

    private float squopCastRadius = 0.24f;                                  // for spherecast
    private float squopCastDistance = 1.5f;                                 // for spherecast (from obove token)
    private Vector3 SquopCastOrigin => transform.position + (Vector3.up * squopCastDistance);       // spherecast downwards

    private float stoppedVelocity = 0.01f;
    private float maxVelocity = 10f;

    public virtual bool IsMoving => tokenRigidbody.velocity.sqrMagnitude > stoppedVelocity;
    public override bool CanMove => base.CanMove && !InCup && !OutOfGame && !IsSquopped;
     
    public bool InCup { get; private set; } = false;
    //[SerializeField] private Color inCupColour = Color.blue;            // for mouse raycast hilight

    private float tokenRepelFactor = 0.06f;                            // when rebounding off other tokens

    private float tokenInPlayDelay = 0.025f;                            // enough time to gain velocity after AddForce
    private float tokenPlayedOutDelay = 0.5f;                           // pause before changing turn


    protected override void OnEnable()
    {
        base.OnEnable();
        TiddlyEvents.OnFlickLiftChanged += OnFlickLiftChanged;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        TiddlyEvents.OnFlickLiftChanged -= OnFlickLiftChanged;
    }

    // if a token is in play, keep checking
    // until it has stopped moving
    private void FixedUpdate()
    {
        // speed limit applies!
        tokenRigidbody.velocity = Vector3.ClampMagnitude(tokenRigidbody.velocity, maxVelocity);

        //Debug.Log($"tokenRigidbody.velocity.magnitude = {tokenRigidbody.velocity.magnitude}"); 

        if (InPlay && !IsMoving)
            StartCoroutine(WaitTokenPlayedOut());
    }

    // hilight on raycast hit
    protected override void TokenSelected(bool raycastHitToken)
    {
        base.TokenSelected(raycastHitToken);

        if (! raycastHitToken)
        {
            ResetHilight();
            return;
        }

        //if (InCup)
        //    Hilight(inCupColour);
        if (IsSquopped)
            Hilight(squoppedColour);
        else if (!CanMove)
            Hilight(cantMoveColour);
        else
            Hilight(canFlickColour);
    }

    // flick on raycast end! (right mouse up)
    protected override void Move()
    {
        base.Move();

        ResetHilight();

        if (! CanMove)
            return;

        var hitDirection = transform.position - RaycastHitPoint;
        var distanceFromCentre = Vector3.Distance(transform.position, RaycastHitPoint);     
        var reflectDirection = Vector3.Reflect(hitDirection, transform.up);
        var flickPoint = transform.position - reflectDirection;             // reflection (vertical) of RaycastHitPoint (for push from underneath token)
        var pushDirection = transform.position - flickPoint;               // push token away from flickPoint, towards centre

        // flick (explosion) force determined by distance from centre
        // zero radius denotes full force applied, regardless of distance from centre
        // explosion point (to make token spin) according to UI slider (from underneath token to create lift)
        tokenRigidbody.AddExplosionForce(flickForce * distanceFromCentre, RaycastHitPoint, 0f, flickLiftModifier * flickLiftFactor); 

        // force (towards centre) determined by distance from centre
        tokenRigidbody.AddForce(pushForce * distanceFromCentre * pushDirection, ForceMode.Impulse);

        if (! InPlay)
            StartCoroutine(WaitTokenInPlay(true));
    }

    // short delay so token has velocity (otherwise immediately stopped)
    // tokenPlayed true if token played by a player, false if because of eg. a collision
    private IEnumerator WaitTokenInPlay(bool tokenPlayed)
    {
        if (tokenPlayed)
            yield return new WaitForSeconds(tokenInPlayDelay);

        InPlay = true;      // start checking for zero velocity
        TurnManager.Instance.TokenPlayed(this);     // stop turn countdown

        if (tokenPlayed) 
            MultiplayerEvents.OnTokenPlayed?.Invoke(this);     // camera follows flicked token, token in play
    }

    // short delay (to pause before changing turn after last token has stopped)
    private IEnumerator WaitTokenPlayedOut()
    {
        if (InPlay)
        {
            InPlay = false;
            yield return new WaitForSeconds(tokenPlayedOutDelay);

            tokenRigidbody.velocity = Vector3.zero;         // to make sure
            TurnManager.Instance.TokenPlayedOut(this);      // changes turn when all tokens have stopped
        }
    }

    // oscillating slider in UI
    private void OnFlickLiftChanged(float liftValue)
    {
        flickLiftFactor = liftValue;
    }

    // set hit tokens to InPlay, in order to wait for all tokens
    // to stop before changing turn
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Token"))
        {
            // tokens push away from each other
            TiddlyToken hitToken = collision.gameObject.GetComponent<TiddlyToken>();
            Vector3 hitDirection = hitToken.transform.position - transform.position;
            hitToken.Repel(hitDirection, tokenRigidbody.velocity.magnitude, Health);

            if (InPlay && ! hitToken.InPlay)
                StartCoroutine(hitToken.WaitTokenInPlay(false));        // hitToken not played by a player
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Cup"))
        {
            InCup = true;
            TurnManager.Instance.AddExtraTurn();
        }
    }

    private void Repel(Vector3 hitDirection, float hitVelocity, float hitHealth)
    {
        tokenRigidbody.AddForce(hitHealth * hitVelocity * tokenRepelFactor * hitDirection, ForceMode.Impulse);
    }

    private bool IsSquopped
    {
        get
        {
            var hits = Physics.SphereCastAll(SquopCastOrigin, squopCastRadius, Vector3.down, squopCastDistance, squopLayers);
            List<RaycastHit> orderedHits = hits.OrderBy(x => x.distance).ToList();

            return (hits.Length > 0 && orderedHits[0].collider.gameObject != gameObject);      // squopped if nearest to raycast origin is not this token!
        }
    }

    //private void OnDrawGizmos()
    //{
        //Gizmos.color = Color.green;
        //Gizmos.DrawWireSphere(SquopCastOrigin, squopRadius);

        //Gizmos.color = Color.magenta;
        //Gizmos.DrawWireSphere(transform.position, squopRadius);

        //Gizmos.color = Color.red;
        //Gizmos.DrawSphere(RaycastHitPoint, 0.01f);
        //Gizmos.DrawLine(RaycastHitPoint, transform.position);

        //Gizmos.color = Color.green;
        //Gizmos.DrawSphere(flickPoint, 0.01f);
        //Gizmos.DrawLine(flickPoint, transform.position);
    //}
}
