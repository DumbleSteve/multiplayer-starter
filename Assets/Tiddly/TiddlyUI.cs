using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TiddlyUI : MonoBehaviour
{
    [SerializeField] private Image flickLiftPanel;
    [SerializeField] private Slider flickLiftSlider;
    [SerializeField] private bool oscillateOnRaycast = false;

    [SerializeField] private Image miniMap;

   
    private int sliderTweenId = 0;
    private float oscillateTime = 1f;
    private float resetTime = 0.25f;

    private bool oscillating = false;

    //private bool isLocalPlayersTurn = false;


    private void OnEnable()
    {
        if (oscillateOnRaycast)
        {
            flickLiftSlider.onValueChanged.AddListener(FlickLiftSliderChanged);

            MultiplayerEvents.OnTokenRaycastHit += OnTokenRaycastHit;
            MultiplayerEvents.OnTokenRaycastEnd += OnTokenRaycastEnd;
        }
        else
        {
            MultiplayerEvents.OnTurnTimeTick += OnTurnTimeTick;
        }

        //MultiplayerEvents.OnIsLocalPlayersTurn += OnIsLocalPlayersTurn;
        MultiplayerEvents.OnLobbyStartGame += OnLobbyStartGame;
    }

    private void OnDisable()
    {
        if (oscillateOnRaycast)
        {
            flickLiftSlider.onValueChanged.RemoveListener(FlickLiftSliderChanged);

            MultiplayerEvents.OnTokenRaycastHit -= OnTokenRaycastHit;
            MultiplayerEvents.OnTokenRaycastEnd -= OnTokenRaycastEnd;
        }
        else
        {
            MultiplayerEvents.OnTurnTimeTick -= OnTurnTimeTick;
        }

        //MultiplayerEvents.OnIsLocalPlayersTurn -= OnIsLocalPlayersTurn;
        MultiplayerEvents.OnLobbyStartGame -= OnLobbyStartGame;
    }

    private void Start()
    {
        flickLiftPanel.gameObject.SetActive(false);
        miniMap.gameObject.SetActive(false);
    }

    private void OnLobbyStartGame(LobbyPlayerData lobbyPlayerData)
    {
        miniMap.gameObject.SetActive(true);
        flickLiftPanel.gameObject.SetActive(true);      // TODO: visible for all players?
    }

    private void FlickLiftSliderChanged(float liftValue)
    {
        if (!oscillateOnRaycast)
            return;

        TiddlyEvents.OnFlickLiftChanged?.Invoke(liftValue);     // 0 to 1
    }

    private void OnTokenRaycastHit(List<PlayerToken> hitTokens)
    {
        if (!oscillateOnRaycast)
            return;

        if (oscillating || hitTokens == null || hitTokens.Count == 0)
            return;

        if (hitTokens[0].CanMove)
            StartSliderOscillate();
        else
            ResetSliderOscillate();
    }

    //private void OnIsLocalPlayersTurn(GamePlayer currentTurnPlayer, bool isLocalPlayersTurn, int numPlayers)
    //{
        //this.isLocalPlayersTurn = isLocalPlayersTurn;
        //flickLiftPanel.gameObject.SetActive(isLocalPlayersTurn);
    //}

    private void OnTurnTimeTick(int maxTime, int timeRemaining, float turnTickInterval)
    {
        if (oscillateOnRaycast)
            return;

        //if (!isLocalPlayersTurn)
        //    return;

        float liftValue = (float)timeRemaining / (float)maxTime;       // 0 - 1

        LeanTween.value(flickLiftSlider.gameObject, flickLiftSlider.value, liftValue, turnTickInterval)
                            .setOnUpdate((float f) => flickLiftSlider.value = f)
                            .setEaseInOutSine()
                            .setOnComplete(() => TiddlyEvents.OnFlickLiftChanged?.Invoke(flickLiftSlider.value));     // reducing token lift
    }

    private void OnTokenRaycastEnd(List<PlayerToken> hitTokens)
    {
        if (!oscillateOnRaycast)
            return;

        ResetSliderOscillate();
    }

    private void StartSliderOscillate()
    {
        if (!oscillateOnRaycast)
            return;

        if (oscillating)
            return;

        ResetSliderOscillate();

        oscillating = true;
        sliderTweenId = LeanTween.value(flickLiftSlider.gameObject, 0f, 1f, oscillateTime)
                                    .setOnUpdate((float f) => flickLiftSlider.value = f)
                                    .setEaseInOutSine()
                                    .setLoopPingPong(0).id;
    }

    private void ResetSliderOscillate()
    {
        if (!oscillateOnRaycast)
            return;

        if (LeanTween.isTweening(sliderTweenId))
            LeanTween.cancel(sliderTweenId);

        // reset to 0
        LeanTween.value(flickLiftSlider.gameObject, flickLiftSlider.value, 0f, resetTime)
                        .setOnUpdate((float f) => flickLiftSlider.value = f)
                        .setEaseInSine()
                        .setEaseInOutSine();

        oscillating = false;
    }
}
