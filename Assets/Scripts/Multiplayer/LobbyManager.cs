using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Services.Authentication;
using Unity.Services.Core;
using Unity.Services.Lobbies;
using Unity.Services.Lobbies.Models;
using UnityEngine;
using System.Linq;

// Singleton class to handle all Unity Lobby and Relay Service functions.
//
// Lobbies do not communicate in realtime, so any updates by host/clients are detected via polling
//
// Works with RelayManager to create (host) or join (client) relays to facilitate peer-to-peer network communication
// this currently takes place when the host starts the game, but could be earlier when players create/join a lobby
//
// Important: RelayManager requires an un-nested NetworkManager in the scene (to start NGO host/clients)
// with Unity Transport Protocol Type set to 'Relay Unity Transport'

public class LobbyManager : MonoBehaviour
{
    public static LobbyManager Instance { get; private set; }       // singleton

    [SerializeField] private PlayerDataSO PlayerData;               // saved player data

    public const string KEY_PLAYER_NAME = "PlayerName";
    public const string KEY_PLAYER_CHARACTER = "Character";

    public const string KEY_LOBBY_GAME_MODE = "GameMode";
    public const string KEY_LOBBY_JOIN_CODE = "JoinCode";     // when host starts a relay, the relay join code is saved in lobby data for clients to use to join relay

    public const int PlayerLimit = 4;                   // game supports max 4

    private float heartBeatTimer = 0f;                  // to regulate heartbeats
    private float heartBeatInterval = 10f;              // lobby goes inactive if no activity for 30 seconds

    private float lobbyUpdateTimer = 0f;                // poll for lobby data changes
    private float lobbyUpdateInterval = 2f; //1.2f;           // 1 per second is minimum allowed poll interval

    public const float RefreshLobbyListInterval = 5f;  // auto lobby list refresh (5 is minimum allowed)
    private float refreshLobbyListTimer = RefreshLobbyListInterval;           // refresh immediately

    private int queryLobbiesLimit = 25;                // restrict number of lobbies retrieved (ordered by newest first)

    private Lobby joinedLobby;
    public Lobby JoinedLobby => joinedLobby;
    public bool IsLobbyHost => joinedLobby != null && joinedLobby.HostId == PlayerId;

    public List<Player> LobbyPlayers => joinedLobby?.Players.OrderBy(p => p.Joined).ToList();       // in order of lobby joined date/time, null if joinedLobby is null

    public int NumberOfPlayers { get; private set; }            // number of players required before game can be started by host

    private Player player;      // Player created when player creates / joins a lobby
    public string LocalPlayerName => PlayerData.PlayerName; // { get; private set; }          // local player, as per AuthenticateUI

    private string PlayerId => AuthenticationService.Instance.PlayerId;
    private bool PlayerSignedIn => (UnityServices.State == ServicesInitializationState.Initialized && AuthenticationService.Instance.IsSignedIn);

    public Player GetLobbyPlayerByName(string playerName) => LobbyPlayers?.FirstOrDefault(p => LobbyPlayerName(p) == playerName);
    public Player LobbyLocalPlayer => GetLobbyPlayerByName(LocalPlayerName);

    public string LobbyPlayerName(Player player) => player.Data[KEY_PLAYER_NAME].Value;
    public Sprite LobbyPlayerCharacter(Player player) => LobbyAssets.Instance.GetSprite(Enum.Parse<PlayerCharacter>(player.Data[KEY_PLAYER_CHARACTER].Value));

    private bool gameStarted = false;       // relay created/joined (host/client created)

    public enum GameMode
    {
        GameMode1,
        GameMode2,
        GameMode3
    }

    public enum PlayerCharacter
    {
        Marine,
        Ninja,
        Zombie
    }

    private void Awake()
    {
        Instance = this;        // singleton
    }

    private void OnApplicationQuit()
    {
        PlayerData.PlayerName = "";         // force player name input next 'session'
        PlayerData.SessionPlayCount = 0; 
    }

    // refresh lobbies, ping joinedLobby and poll for changes
    private void Update()
    {
        if (!PlayerSignedIn)
            return;

        AutoRefreshLobbyList();         // check for updates

        if (joinedLobby == null)
            return;

        if (IsLobbyHost)
            LobbyHeartBeatPing();            // keep alive if inactive

        if (!gameStarted)
            PollForLobbyChanges();
    }

    public async void Authenticate(string playerName)
    {
        if (string.IsNullOrEmpty(playerName))
        {
            MultiplayerEvents.OnAuthenticateError?.Invoke($"No Player Name entered!");
            return; 
        }

        try
        {
            PlayerData.PlayerName = "";     // in case of error (see below)

            try
            {
                InitializationOptions initializationOptions = new();
                initializationOptions.SetProfile(playerName);
                // send request over internet to initialise the API
                await UnityServices.InitializeAsync(initializationOptions);
            }
            catch (AuthenticationException ae)
            {
                // prolly illegal profile name
                // report to user
                MultiplayerEvents.OnAuthenticateError?.Invoke($"'{playerName}' is not a valid Player Name");
                return;
            }

            // listen for sign-in events
            AuthenticationService.Instance.SignedIn += () =>
            {
                PlayerData.PlayerName = playerName;
                MultiplayerEvents.OnAuthenticated?.Invoke(playerName, PlayerId);
                RefreshLobbyList();
            };

            AuthenticationService.Instance.SignInFailed += (errorResponse) =>
            {
                PlayerData.PlayerName = "";
                MultiplayerEvents.OnAuthenticateError?.Invoke($"Sign in failed: {errorResponse.ErrorCode}");
                Debug.LogError($"Sign in anonymously failed with error code: {errorResponse.ErrorCode}");
            };

            AuthenticationService.Instance.SignedOut += () =>
            {
                PlayerData.PlayerName = "";
                Debug.Log("Signed Out!");
            };

            // sign in!
            await AuthenticationService.Instance.SignInAnonymouslyAsync();
        }
        catch (LobbyServiceException le)
        {
            MultiplayerEvents.OnAuthenticateError?.Invoke($"'{le.Message}'");
            Debug.LogError($"Authenticate: {le}");
        }
    }

    public async void RefreshLobbyList()
    {
        if (!PlayerSignedIn)
            return;

        refreshLobbyListTimer = RefreshLobbyListInterval;       // reset

        try
        {
            QueryLobbiesOptions queryLobbiesOptions = new QueryLobbiesOptions
            {
                Count = queryLobbiesLimit,
                Filters = new List<QueryFilter>
                {
                    new QueryFilter(QueryFilter.FieldOptions.AvailableSlots, "0", QueryFilter.OpOptions.GT),
                    //new QueryFilter(QueryFilter.FieldOptions.S1, "GameMode1", QueryFilter.OpOptions.EQ)      // index useful for more complex use cases
                },

                // order by newest lobbies first
                Order = new List<QueryOrder>
                {
                    new QueryOrder(false, QueryOrder.FieldOptions.Created)
                }
            };

            QueryResponse lobbyListQueryResponse = await Lobbies.Instance.QueryLobbiesAsync(queryLobbiesOptions);
            MultiplayerEvents.OnLobbyListChanged?.Invoke(lobbyListQueryResponse.Results);
        }
        catch (LobbyServiceException e)
        {
            Debug.LogError($"RefreshLobbyList: {e}");
        }
    }

    // host
    public async void CreateLobby(string lobbyName, int maxPlayers, bool isPrivate, GameMode gameMode)
    {
        if (! PlayerSignedIn)
            return;

        if (string.IsNullOrEmpty(lobbyName))
            return;

        try
        {
            NumberOfPlayers = maxPlayers;

            player = CreatePlayer();

            CreateLobbyOptions createLobbyOptions = new CreateLobbyOptions
            {
                IsPrivate = isPrivate,

                // generic data that each player needs - eg. name
                // in data for player creating this lobby
                Player = player,

                // generic data for the lobby
                Data = new Dictionary<string, DataObject>
                {
                    { KEY_LOBBY_GAME_MODE, new DataObject(DataObject.VisibilityOptions.Public, gameMode.ToString()) },  //, DataObject.IndexOptions.S1) }       // index can be used for filters
                    { KEY_LOBBY_JOIN_CODE, new DataObject(DataObject.VisibilityOptions.Member, "0") }                 // visible only inside lobby
                }
            };

            joinedLobby = await LobbyService.Instance.CreateLobbyAsync(lobbyName, maxPlayers, createLobbyOptions);
            MultiplayerEvents.OnJoinedLobby?.Invoke(joinedLobby, IsLobbyHost, LocalPlayerName);

            //SubscribeToLobbyEvents();
        }
        catch (LobbyServiceException e)
        {
            Debug.LogError($"CreateLobby {e}");
        }
    }

    public void JoinLobby(Lobby lobby)
    {
        if (!PlayerSignedIn)
            return;

        NumberOfPlayers = lobby.MaxPlayers;

        JoinLobbyById(lobby.Id);
    }

    private async void JoinLobbyById(string lobbyId)
    {
        if (!PlayerSignedIn)
            return;

        try
        {
            player = CreatePlayer();

            JoinLobbyByIdOptions joinLobbyByIdOptions = new JoinLobbyByIdOptions
            {
                // generic data that each player needs - eg. name
                // int data for player joining this lobby
                Player = player
            };

            joinedLobby = await Lobbies.Instance.JoinLobbyByIdAsync(lobbyId, joinLobbyByIdOptions);
            MultiplayerEvents.OnJoinedLobby?.Invoke(joinedLobby, IsLobbyHost, LocalPlayerName);

            //SubscribeToLobbyEvents();
        }
        catch (LobbyServiceException e)
        {
            Debug.LogError($"JoinLobbyById {e}");
        }
    }

    private async void JoinLobbyByCode(string lobbyCode)
    {
        if (!PlayerSignedIn)
            return;

        try
        {
            player = CreatePlayer();

            JoinLobbyByCodeOptions joinLobbyByCodeOptions = new JoinLobbyByCodeOptions
            {
                // generic data that each player needs - eg. name
                // int data for player joining this lobby
                Player = player
            };

            joinedLobby = await Lobbies.Instance.JoinLobbyByCodeAsync(lobbyCode, joinLobbyByCodeOptions);
            MultiplayerEvents.OnJoinedLobby?.Invoke(joinedLobby, IsLobbyHost, LocalPlayerName);

            //SubscribeToLobbyEvents();
        }
        catch (LobbyServiceException e)
        {
            Debug.LogError($"JoinLobbyByCode {e}");
        }
    }

    private async void QuickJoinLobby()
    {
        try
        {
            QuickJoinLobbyOptions options = new QuickJoinLobbyOptions
            {
                // generic data that each player needs - eg. name
                // int data for player joining this lobby
                Player = player
            };

            joinedLobby = await LobbyService.Instance.QuickJoinLobbyAsync(options);
            MultiplayerEvents.OnJoinedLobby?.Invoke(joinedLobby, IsLobbyHost, LocalPlayerName);
        }
        catch (LobbyServiceException e)
        {
            Debug.LogError($"QuickJoinLobby {e}");
        }
    }

    // generic data that each player needs - eg. name
    // init data for player creating or joining this lobby
    private Player CreatePlayer()
    {
        return new Player(PlayerId, null, new Dictionary<string, PlayerDataObject>
                    {
                        { KEY_PLAYER_NAME, new PlayerDataObject(PlayerDataObject.VisibilityOptions.Public, LocalPlayerName) },
                        { KEY_PLAYER_CHARACTER, new PlayerDataObject(PlayerDataObject.VisibilityOptions.Public, PlayerCharacter.Marine.ToString()) }
                    });
    }

    // update lobby data
    private async void ChangeLobbyGameMode(GameMode gameMode)
    {
        try
        {
            joinedLobby = await Lobbies.Instance.UpdateLobbyAsync(joinedLobby.Id, new UpdateLobbyOptions
            {
                // only need to include Data
                // dictionary keys that need to be modified
                Data = new Dictionary<string, DataObject>
                {
                    { KEY_LOBBY_GAME_MODE, new DataObject(DataObject.VisibilityOptions.Public, gameMode.ToString()) }
                }
            });

            MultiplayerEvents.OnLobbyGameModeChanged?.Invoke(joinedLobby);
        }
        catch (LobbyServiceException e)
        {
            Debug.LogError($"UpdateLobbyGameMode {e}");
        }
    }

    // update local player's name
    private async void ChangePlayerName(string newPlayerName)
    {
        if (joinedLobby == null)
            return;

        try
        {
            PlayerData.PlayerName = newPlayerName;

            joinedLobby = await LobbyService.Instance.UpdatePlayerAsync(joinedLobby.Id, PlayerId, new UpdatePlayerOptions
            {
                Data = new Dictionary<string, PlayerDataObject>
                {
                    { KEY_PLAYER_NAME, new PlayerDataObject(PlayerDataObject.VisibilityOptions.Public, LocalPlayerName) }
                }
            });

            MultiplayerEvents.OnLobbyUpdated?.Invoke(joinedLobby);
        }
        catch (LobbyServiceException e)
        {
            Debug.LogError($"UpdatePlayerName {e}");
        }
    }

    //public Sprite GetPlayerCharacterSprite(Player player)
    //{
    //    PlayerCharacter playerCharacter = Enum.Parse<PlayerCharacter>(player.Data[KEY_PLAYER_CHARACTER].Value);
    //    return LobbyAssets.Instance.GetSprite(playerCharacter);
    //}

    public async void ChangePlayerCharacter(PlayerCharacter playerCharacter)
    {
        if (joinedLobby == null)
            return;

        try
        {
            joinedLobby = await LobbyService.Instance.UpdatePlayerAsync(joinedLobby.Id, PlayerId, new UpdatePlayerOptions
            {
                Data = new Dictionary<string, PlayerDataObject>
                {
                    { KEY_PLAYER_CHARACTER, new PlayerDataObject(PlayerDataObject.VisibilityOptions.Public, playerCharacter.ToString()) }
                }
            });

            MultiplayerEvents.OnLobbyUpdated?.Invoke(joinedLobby);
        }
        catch (LobbyServiceException e)
        {
            Debug.LogError($"ChangePlayerCharacter: {e}");
        }
    }

    public async void LeaveLobby()
    {
        if (!PlayerSignedIn)
            return;

        if (joinedLobby == null)
            return;

        try
        {
            await LobbyService.Instance.RemovePlayerAsync(joinedLobby.Id, PlayerId);

            joinedLobby = null;

            MultiplayerEvents.OnLeftLobby?.Invoke();
        }
        catch (LobbyServiceException e)
        {
            Debug.LogError($"LeaveLobby {e}");
        }
    }

    public async void KickPlayer(string playerId)
    {
        if (!IsLobbyHost)
            return;

        try
        {
            await LobbyService.Instance.RemovePlayerAsync(joinedLobby.Id, playerId);
        }
        catch (LobbyServiceException e)
        {
            Debug.LogError($"KickPlayer {e}");
        }
    }

    // TODO: delete lobby when all players linked up?
    private async void DeleteLobby()
    {
        if (joinedLobby == null)
            return;

        try
        {
            await LobbyService.Instance.DeleteLobbyAsync(joinedLobby.Id);
            joinedLobby = null;
        }
        catch (LobbyServiceException e)
        {
            Debug.LogError($"DeleteLobby {e}");
        }
    }

    public void StartGame()
    {
        if (!IsLobbyHost) // || relayManager == null)
            return;

        // create relay (and start host), saving join code in lobby data for
        // clients to pick up when polling for lobby changes,
        // at which point they use the join code to join the relay (and start client)
        CreateRelay(true); 
    }

    // RelayManager creates a relay and starts a host, returning a join code
    // which is saved in the lobby data for the clients to pick up as the lobby is polled for changes
    // at which point they each join the relay with the join code.
    private async void CreateRelay(bool startGame)
    {
        if (!IsLobbyHost || joinedLobby == null) // || relayManager == null)
            return;

        if (startGame)
        {
            gameStarted = true;     // stop polling for lobby changes
            MultiplayerEvents.OnLobbyStartGame?.Invoke(new LobbyPlayerData { PlayerName = LocalPlayerName,
                                                PlayerCharacter = LobbyPlayerCharacter(player), Joined = player.Joined });
        }

        try
        {
            string relayCode = await RelayManager.Instance.CreateRelay();      // starts NGO host!

            joinedLobby = await Lobbies.Instance.UpdateLobbyAsync(joinedLobby.Id, new UpdateLobbyOptions
            {
                // save the relayCode in the lobby data to be picked up by the clients in order to join this relay seamlessly
                Data = new Dictionary<string, DataObject>
                {
                    { KEY_LOBBY_JOIN_CODE, new DataObject(DataObject.VisibilityOptions.Member, relayCode) }        // visible only inside lobby!
                }
            });
        }
        catch (LobbyServiceException e)
        {
            Debug.LogError($"CreateRelay {e}");
        }
    }

    // RelayManager joins a relay using the join code created
    // when the relay was created by the host on game start
    // relayJoinCode is detected via PollForLobbyChanges
    private async void JoinRelay(string relayJoinCode, bool startGame)
    {
        if (IsLobbyHost) // || relayManager == null)    // lobby host has already joined relay
            return;

        if (joinedLobby == null)
            return;

        if (relayJoinCode == "0" || string.IsNullOrEmpty(relayJoinCode))
            return;

        if (startGame)
        {
            gameStarted = true;     // stop polling for lobby changes
            MultiplayerEvents.OnLobbyStartGame?.Invoke(new LobbyPlayerData { PlayerName = LocalPlayerName,
                                                PlayerCharacter = LobbyPlayerCharacter(player), Joined = player.Joined  });       // TODO: delete lobby? not here - causes problems for clients joining!
        }

        try
        {
            await RelayManager.Instance.JoinRelay(relayJoinCode);          // starts NGO client!
        }
        catch (LobbyServiceException e)
        {
            Debug.LogError($"JoinRelay {e}");
        }
    }

    // keep lobby alive until all players have joined
    private async void LobbyHeartBeatPing()
    {
        if (!PlayerSignedIn)
            return;

        if (!IsLobbyHost)
            return;

        if (joinedLobby == null)
            return;

        try
        {
            heartBeatTimer -= Time.deltaTime;
            if (heartBeatTimer <= 0)
            {
                heartBeatTimer = heartBeatInterval;     // reset for next beat
                await LobbyService.Instance.SendHeartbeatPingAsync(joinedLobby.Id);
            }
        }
        catch (LobbyServiceException e)
        {
            Debug.LogError($"LobbyHeartBeatPing: {e}");
        }
    }

    // poll for any changes (by other players) to lobby data
    private async void PollForLobbyChanges()
    {
        if (!PlayerSignedIn)
            return;

        if (joinedLobby == null)
            return;

        if (gameStarted)
            return;

        lobbyUpdateTimer -= Time.deltaTime;
        if (lobbyUpdateTimer <= 0)
        {
            try
            {
                lobbyUpdateTimer = lobbyUpdateInterval;     // reset for next poll

                joinedLobby = await LobbyService.Instance.GetLobbyAsync(joinedLobby.Id);
                MultiplayerEvents.OnLobbyUpdated?.Invoke(joinedLobby);

                if (! IsPlayerInLobby())
                {
                    // player has been kicked out of this lobby
                    MultiplayerEvents.OnKickedFromLobby?.Invoke();
                    joinedLobby = null;
                }
                else
                {
                    // join relay (start client) if lobby data KEY_JOIN_CODE has been set to the relay join code
                    // (when host created a relay on game start)
                    string relayJoinCode = joinedLobby.Data[KEY_LOBBY_JOIN_CODE].Value;

                    if (relayJoinCode != "0" && !string.IsNullOrEmpty(relayJoinCode))
                    {
                         JoinRelay(relayJoinCode, true);
                    }
                }
            }
            catch (LobbyServiceException e)
            {
                Debug.LogError($"PollForLobbyChanges: {e}");

                // prolly lobby not found - eg. host left
                MultiplayerEvents.OnKickedFromLobby?.Invoke();
                joinedLobby = null;
            }
        }
    }

    private void AutoRefreshLobbyList()
    {
        if (! PlayerSignedIn)
            return;

        refreshLobbyListTimer -= Time.deltaTime;
        if (refreshLobbyListTimer <= 0f)
        {
            RefreshLobbyList();     // resets refreshLobbyListTimer
        }
    }

    public void ChangeLobbyGameMode()
    {
        if (!IsLobbyHost)
            return;

        GameMode gameMode = Enum.Parse<GameMode>(joinedLobby.Data[KEY_LOBBY_GAME_MODE].Value);
        gameMode = CycleGameMode(gameMode);

        ChangeLobbyGameMode(gameMode);
    }

    public GameMode CycleGameMode(GameMode gameMode)
    {
        switch (gameMode)
        {
            default:
            case GameMode.GameMode1:
                return GameMode.GameMode2;

            case GameMode.GameMode2:
                return GameMode.GameMode3;

            case GameMode.GameMode3:
                return GameMode.GameMode1;
        }
    }

    private bool IsPlayerInLobby()
    {
        if (joinedLobby != null && joinedLobby.Players != null)
        {
            foreach (Player player in joinedLobby.Players)
            {
                if (player.Id == PlayerId)
                {
                    // This player is in this lobby
                    return true;
                }
            }
        }
        return false;
    }

    // lobby events

    //private async void SubscribeToLobbyEvents()
    //{
    //    if (joinedLobby == null)
    //        return;

    //    var callbacks = new LobbyEventCallbacks();
    //    callbacks.LobbyChanged += OnLobbyChanged;
    //    callbacks.KickedFromLobby += OnKickedFromLobby;
    //    callbacks.LobbyEventConnectionStateChanged += OnLobbyEventConnectionStateChanged;
    //    try
    //    {
    //        var lobbyEvents = await Lobbies.Instance.SubscribeToLobbyEventsAsync(joinedLobby.Id, callbacks);
    //    }
    //    catch (LobbyServiceException ex)
    //    {
    //        switch (ex.Reason)
    //        {
    //            case LobbyExceptionReason.AlreadySubscribedToLobby:
    //                    Debug.LogWarning($"Already subscribed to lobby[{joinedLobby.Id}]. We did not need to try and subscribe again. Exception Message: {ex.Message}");
    //                    break;
    //            case LobbyExceptionReason.SubscriptionToLobbyLostWhileBusy:
    //                    Debug.LogError($"Subscription to lobby events was lost while it was busy trying to subscribe. Exception Message: {ex.Message}");
    //                    throw;
    //            case LobbyExceptionReason.LobbyEventServiceConnectionError:
    //                    Debug.LogError($"Failed to connect to lobby events. Exception Message: {ex.Message}");
    //                    throw;
    //            default:
    //                throw;
    //        }
    //    }
    //}

    //private void OnLobbyEventConnectionStateChanged(LobbyEventConnectionState state)
    //{
    //    Debug.Log($"<color=yellow>OnLobbyEventConnectionStateChanged state = {state} </color>");
    //}

    //private void OnKickedFromLobby()
    //{
    //    Debug.Log($"<color=orange> OnKickedFromLobby </color>");
    //    GameEvents.OnKickedFromLobby?.Invoke();
    //    joinedLobby = null;
    //}

    //private void OnLobbyChanged(ILobbyChanges changes)
    //{
    //    if (joinedLobby == null || changes.LobbyDeleted)
    //    {
    //        // prolly lobby not found - eg. host left
    //        GameEvents.OnKickedFromLobby?.Invoke();
    //        joinedLobby = null;
    //    }
    //    else
    //    {
    //        changes.ApplyToLobby(joinedLobby);
    //    }

    //    Debug.Log($"<color=yellow>OnLobbyChanged: changes = {changes} </color>");
    //}

    //private async void MigrateLobbyHost(int newHostIndex)
    //{
    //    try
    //    {
    //        Lobby lobby = await Lobbies.Instance.UpdateLobbyAsync(hostLobby.Id, new UpdateLobbyOptions
    //        {
    //            HostId = joinedLobby.Players[newHostIndex].Id
    //        });

    //        joinedLobby = hostLobby = lobby;
    //        //GameEvents.OnJoinedLobby?.Invoke(joinedLobby);

    //        PrintPlayers(hostLobby);
    //    }
    //    catch (LobbyServiceException e)
    //    {
    //        Debug.Log($"MigrateLobbyHost {e}");
    //    }
    //}
}

// player data extracted from lobby - used for display purposes
public struct LobbyPlayerData
{
    public string PlayerName;
    public Sprite PlayerCharacter;
    public DateTime Joined;
}
