using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyAssets : MonoBehaviour
{
    public static LobbyAssets Instance { get; private set; }        // singleton

    [SerializeField] private Sprite marineSprite;
    [SerializeField] private Sprite ninjaSprite;
    [SerializeField] private Sprite zombieSprite;

    private void Awake()
    {
        Instance = this;                                        // singleton
    }

    public Sprite GetSprite(LobbyManager.PlayerCharacter playerCharacter)
    {
        return playerCharacter switch
        {
            LobbyManager.PlayerCharacter.Ninja => ninjaSprite,
            LobbyManager.PlayerCharacter.Zombie => zombieSprite,
            _ => marineSprite,
        };
    }

}
