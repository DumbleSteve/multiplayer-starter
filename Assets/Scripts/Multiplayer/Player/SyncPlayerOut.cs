using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;


// Player out of game network synchronisation

public class SyncPlayerOut : NetworkBehaviour
{
    public void SetPlayerOut(int outGamePlayerIndex, bool isWinner)
    {
        //Debug.Log($"<color=white> SetPlayerOut outOwnerClientId {outOwnerClientId}</color>");
        PlayerOutServerRPC(outGamePlayerIndex, isWinner);                // server to all clients
    }

    // client to server
    // this players turn - call to all clients (of this player)
    [ServerRpc(RequireOwnership = false)]
    private void PlayerOutServerRPC(int outGamePlayerIndex, bool isWinner)
    {
        //Debug.Log($"<color=yellow> PlayerOutServerRPC outOwnerClientId {outOwnerClientId}</color>");
        PlayerOutClientRPC(outGamePlayerIndex, isWinner);                // server to all clients
    }

    // server to all clients (of this player)
    [ClientRpc]
    private void PlayerOutClientRPC(int outGamePlayerIndex, bool isWinner)
    {
        //Debug.Log($"<color=orange> PlayerOutClientRPC outOwnerClientId {outGamePlayerIndex}</color>");
        NetworkInterface.Instance.ResumePlayerOut(outGamePlayerIndex, isWinner); 
    }
}
