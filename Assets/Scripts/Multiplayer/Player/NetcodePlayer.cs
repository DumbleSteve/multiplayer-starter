﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(SyncPlayerTurns))]      // to sync change of player turn
[RequireComponent(typeof(SyncPlayerOut))]        // to sync player out of game (therefore also game over)

public class NetcodePlayer : NetworkBehaviour
{
    //private NetworkVariable<int> networkInt;
    //private NetworkVariable<float> networkFloat;
    //private NetworkVariable<bool> networkBool;

    // create an INetworkSerializable struct that can be used as a variable that is synced across the network
    // value types only!
    //private NetworkVariable<MyCustomData> randomNetworkData = new NetworkVariable<MyCustomData>(
    //    new MyCustomData
    //    {
    //        _int = 56,
    //        _bool = true,
    //        _message = "",
    //    }, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);       // server can always write

    public NetworkVariable<FixedString32Bytes> PlayerName { get; private set; } = new("", NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);     // set by local player on authentication
    public bool PlayerNameSet { get; private set; }

    public LobbyPlayerData LobbyPlayerData { get; private set; }          // from lobby

    // a NetcodePlayer can control more than one GamePlayer (if lobby players < TurnManager colours)
    public List<GamePlayer> GamePlayers { get; private set; } = new();      // TODO: not currently used

    private SyncPlayerTurns syncPlayerTurns;          // required
    private SyncPlayerOut syncPlayerOut;              // required


    // NOTE: use this instead of Start or Awake for NetworkBehaviours!
    public override void OnNetworkSpawn()
    {
        syncPlayerTurns = GetComponent<SyncPlayerTurns>();
        syncPlayerOut = GetComponent<SyncPlayerOut>();

        PlayerName.OnValueChanged += PlayerNameChanged;         // game starts when all player names have been set across network

        if (IsHost)
        {
            NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnected;
            NetworkManager.Singleton.OnClientDisconnectCallback += OnClientDisconnected;
        }

        // cache player and set/sync all player names across network
        NetworkInterface.Instance.RegisterNetcodePlayer(this);
        base.OnNetworkSpawn();
    }

    public override void OnNetworkDespawn()
    {
        PlayerName.OnValueChanged -= PlayerNameChanged;

        if (IsHost)
        {
            NetworkManager.Singleton.OnClientConnectedCallback -= OnClientConnected;
            NetworkManager.Singleton.OnClientDisconnectCallback -= OnClientDisconnected;
        }

        NetworkInterface.Instance.RemoveNetcodePlayer(OwnerClientId);      // eg. add to UI / reactivate buttons etc.
        base.OnNetworkDespawn();
    }


    private void PlayerNameChanged(FixedString32Bytes previousValue, FixedString32Bytes newValue)
    {
        PlayerNameSet = !string.IsNullOrEmpty(newValue.ToString());
        NetworkInterface.Instance.StartIfAllPlayerNamesSynced();      // netcode and GamePlayers linked, PlayerTokens spawned etc. when all player names have been set
    }

    // called when all netcode players have been spawned
    public void SetPlayerName()
    {
        //Debug.Log($"SetPlayerName: {OwnerClientId}  IsOwner {IsOwner}");

        if (IsOwner)        // PlayerName network variable writeable by Owner
            PlayerName.Value = LobbyManager.Instance.LocalPlayerName;
    }

    // called when all NetcodePlayers have been spawned
    // save player data from lobby
    public void SetLobbyPlayerData(LobbyPlayerData lobbyPlayerData)
    {
        LobbyPlayerData = lobbyPlayerData;
    }

    // called when all NetcodePlayers have been spawned
    // attach GamePlayer to this NetcodePlayer and vice versa
    public void LinkToGamePlayer(GamePlayer gamePlayer)
    {
        GamePlayers.Add(gamePlayer);        // TODO: GamePlayers not currently used
        gamePlayer.LinkNetcodePlayer(this);
    }

#region player turn synchronisation

    // called by NetworkInterface for all players at turn start
    // once the next turn player has been set by TurnManager
    public void ChangePlayerTurn(ulong turnOwnerClientId, int currentGamePlayerTurnIndex)
    {
        //Debug.Log($"<color=white> ChangePlayerTurn turnOwnerClientId {turnOwnerClientId} currentPlayerTurnIndex {currentPlayerTurnIndex}</color>");
        syncPlayerTurns.ChangePlayerTurn(turnOwnerClientId, currentGamePlayerTurnIndex);                // server to all clients
    }

#endregion

#region player out of game synchronisation

    public void SetPlayerOut(int outGamePlayerIndex, bool isWinner)
    {
        //Debug.Log($"<color=white> ChangePlayerTurn turnOwnerClientId {turnOwnerClientId} currentPlayerTurnIndex {currentPlayerTurnIndex}</color>");
        syncPlayerOut.SetPlayerOut(outGamePlayerIndex, isWinner);                // server to all clients
    }

#endregion

    // response to NetworkManager client connected event
    private void OnClientConnected(ulong clientID)
    {
    }

    // response to NetworkManager client disconnected event
    // if host left, disconnect all clients
    private void OnClientDisconnected(ulong clientID)
    {
    }
}
