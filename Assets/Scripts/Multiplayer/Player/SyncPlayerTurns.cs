﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;


// Player change of turn network synchronisation

public class SyncPlayerTurns : NetworkBehaviour
{
    private void OnEnable()
    {
        MultiplayerEvents.OnGameReadyToPlay += OnGameReadyToPlay;
    }
    private void OnDisable()
    {
        MultiplayerEvents.OnGameReadyToPlay -= OnGameReadyToPlay;
    }

    // host starts the turn machine!
    private void OnGameReadyToPlay()
    {
        if (IsOwner && IsHost)
        {
            MultiplayerEvents.OnTurnStart?.Invoke(false);      // set first player and start turn for all players
        }
    }

    public void ChangePlayerTurn(ulong turnOwnerClientId, int currentGamePlayerTurnIndex)
    {
        //Debug.Log($"<color=white> SetPlayerTurn turnOwnerClientId {turnOwnerClientId} currentPlayerTurnIndex {currentPlayerTurnIndex}</color>");
        PlayerTurnServerRPC(turnOwnerClientId, currentGamePlayerTurnIndex);                // server to all clients
    }

    // client to server
    // this players turn - call to all clients (of this player)
    [ServerRpc(RequireOwnership = false)]
    private void PlayerTurnServerRPC(ulong turnOwnerClientId, int currentGamePlayerTurnIndex)
    {
        //Debug.Log($"<color=yellow> PlayerTurnServerRPC turnOwnerClientId {turnOwnerClientId} currentPlayerTurnIndex {currentPlayerTurnIndex}</color>");
        PlayerTurnClientRPC(turnOwnerClientId, currentGamePlayerTurnIndex);                // server to all clients
    }

    // server to all clients (of this player)
    [ClientRpc]
    private void PlayerTurnClientRPC(ulong turnOwnerClientId, int currentGamePlayerTurnIndex)
    {
        if (!IsOwner)
            return;

        bool thisPlayersTurn = OwnerClientId == turnOwnerClientId;
        //Debug.Log($"<color=orange> PlayerTurnClientRPC turnOwnerClientId {turnOwnerClientId} currentPlayerTurnIndex {currentPlayerTurnIndex} thisPlayersTurn {thisPlayersTurn}</color>");
        NetworkInterface.Instance.ResumeStartTurn(currentGamePlayerTurnIndex, thisPlayersTurn);    // 'your turn' UI, enable/disable raycast, etc
    }
}
