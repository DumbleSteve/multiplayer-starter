using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Unity.Services.Lobbies.Models;


// handles networking syncing - via ClientRPCs
// in conjunction with TurnManager singleton
// typically simply 'forwards' key network 'events' onto all clients via local events
//
// contains a list of NetcodePlayers, populated as each player is spawned

public class NetworkInterface : MonoBehaviour
{
    public static NetworkInterface Instance { get; private set; }       // singleton

    [SerializeField] private AudioClip nextPlayerAudio;

    private readonly List<NetcodePlayer> netcodePlayers = new(); // add as spawned - host (as creator of relay) will be first, clients in random order (ie. as spawned)
    public List<NetcodePlayer> OrderedNetcodePlayers => netcodePlayers.OrderBy(p => p.OwnerClientId).ToList();
    public NetcodePlayer NetcodePlayerById(ulong ownerClientId) => netcodePlayers.FirstOrDefault(x => x.OwnerClientId == ownerClientId);

    public int NetcodePlayerCount => netcodePlayers.Count;

    private bool AllNetcodePlayersSpawned => NetcodePlayerCount == LobbyManager.Instance.NumberOfPlayers;
    private int PlayerNameCount => netcodePlayers.Count(p => p.PlayerNameSet);
    private bool AllNetcodePlayerNamesSynced => PlayerNameCount == LobbyManager.Instance.NumberOfPlayers;

    public bool IsLocalPlayersTurn { get; private set; }            // actions disabled if not local player's turn


    private void Awake()
    {
        Instance = this;        // singleton
    }


    #region player turns

    public void SyncPlayerTurns(ulong turnOwnerClientId, int currentGamePlayerTurnIndex)
    {
        // update 'turn status' for all players (any order)
        foreach (var player in netcodePlayers)
        {
            player.ChangePlayerTurn(turnOwnerClientId, currentGamePlayerTurnIndex);     // ServerRPC -> ClientRPC (OnTurnChanged callback event)
        }
    }

    // called by NetcodePlayerTurns client RPC
    // fire local events to start turn and update 'your turn' status of all players
    public void ResumeStartTurn(int currentGamePlayerTurnIndex, bool isPlayersTurn)
    {
        //Debug.Log($"<color=lime>NetworkTurnManager.NetworkStartTurn currentPlayerTurnIndex {currentGamePlayerTurnIndex} isPlayersTurn {isPlayersTurn}</color>");

        // set current GamePlayer
        GamePlayer currentTurnPlayer = TurnManager.Instance.GamePlayerByIndex(currentGamePlayerTurnIndex); 
        MultiplayerEvents.OnTurnChanged?.Invoke(currentTurnPlayer);

        IsLocalPlayersTurn = isPlayersTurn;
        MultiplayerEvents.OnIsLocalPlayersTurn?.Invoke(currentTurnPlayer, isPlayersTurn, NetcodePlayerCount);

        if (nextPlayerAudio != null)
            AudioSource.PlayClipAtPoint(nextPlayerAudio, currentTurnPlayer.PlayerTokens.transform.position);
    }

    #endregion


    #region player out (also game over)

    public void SyncPlayerOut(ulong outPlayerClientId, int outGamePlayerIndex, bool isWinner)
    {
        var outPlayer = NetcodePlayerById(outPlayerClientId);
        outPlayer.SetPlayerOut(outGamePlayerIndex, isWinner);     // ServerRPC -> ClientRPC
    }

    // called by NetcodePlayerOut client RPC
    // fire local events to update UI etc
    public void ResumePlayerOut(int outGamePlayerIndex, bool isWinner)
    {
        //Debug.Log($"<color=lime>ResumePlayerOut outPlayerIndex {outGamePlayerIndex}  isWinner {isWinner}</color>");

        GamePlayer outPlayer = TurnManager.Instance.GamePlayerByIndex(outGamePlayerIndex);
        outPlayer.SetPlayerOutOfGame(isWinner);

        if (isWinner)
            MultiplayerEvents.OnGameOver?.Invoke(outPlayer);
        else
            MultiplayerEvents.OnTurnStart?.Invoke(false);               // set next player and start turn for all players
    }

    #endregion


    #region netcode players join/leave

    // cache newly spawned NetcodePlayer and set all player names (network variable)
    // when all NetcodePlayers are in-game
    public void RegisterNetcodePlayer(NetcodePlayer netcodePlayer)
    {
        netcodePlayers.Add(netcodePlayer);
        //Debug.Log($"RegisterNetcodePlayer: {netcodePlayer.OwnerClientId}  AllNetcodePlayersSpawned {AllNetcodePlayersSpawned}");

        // if all NetcodePlayers are in game, set all player names before assigning lobby player data and then spawning PlayerTokens!
        if (AllNetcodePlayersSpawned)
        {
            // set all player names
            foreach (var player in netcodePlayers)
            {
                if (player.IsOwner)
                    player.SetPlayerName();     // owner sets player name network variable (to sync across network)
            }
        }
    }

    // once all player names have been set, use names to get lobby player data
    // link GamePlayers -> start game!
    public void StartIfAllPlayerNamesSynced()
    {
        //Debug.Log($"StartIfAllPlayerNamesSet: AllNetcodePlayerNamesSet {AllNetcodePlayerNamesSet}");
        if (! AllNetcodePlayerNamesSynced)
            return;

        LinkLobbyNetcodePlayers();      // get name, sprite and joined datetime from lobby player
        LinkGameNetcodePlayers();       

        MultiplayerEvents.OnGameReadyToPlay?.Invoke();
    }

    #endregion

    // when all NetcodePlayers are in the game and all player names have been synced across network,
    // set player name, sprite and joined datetime for all netcode players
    private List<LobbyPlayerData> LinkLobbyNetcodePlayers()
    {
        if (!AllNetcodePlayersSpawned)      // shouldn't be here yet if not! (event sequence)
            return null;

        if (!AllNetcodePlayerNamesSynced)      // shouldn't be here yet if not! (event sequence)
            return null;

        List<LobbyPlayerData> lobbyPlayers = new();
        List<NetcodePlayer> orderedPlayers = OrderedNetcodePlayers;       // in the order the players NetcodePlayers were spawned (ie. when they picked up join code)

        for (int playerIndex = 0; playerIndex < LobbyManager.Instance.NumberOfPlayers; playerIndex++)
        {
            NetcodePlayer netcodePlayer = orderedPlayers[playerIndex];

            // matching by lobby player name, set character sprite and joined time when new netcode player is spawned
            string netcodePlayerName = netcodePlayer.PlayerName.Value.ToString();     // network variable
            Player lobbyPlayer = LobbyManager.Instance.GetLobbyPlayerByName(netcodePlayerName);

            //Debug.Log($"<color=orange>LinkLobbyNetcodePlayers: playerName '{netcodePlayerName}' {(lobbyPlayer != null ? "Found player!" : "Player not found!!")}</color>");

            //string playerName = LobbyManager.Instance.LobbyPlayerName(lobbyPlayer);
            Sprite playerCharacter = LobbyManager.Instance.LobbyPlayerCharacter(lobbyPlayer);

            LobbyPlayerData lobbyPlayerData = new() { PlayerName = netcodePlayerName, PlayerCharacter = playerCharacter, Joined = lobbyPlayer.Joined };
            lobbyPlayers.Add(lobbyPlayerData);
            netcodePlayer.SetLobbyPlayerData(lobbyPlayerData);
        }

        return lobbyPlayers;
    }

    // when all NetcodePlayers are in the game and all GamePlayers have been constructed
    // link each NetcodePlayer to its corresponding GamePlayer
    private void LinkGameNetcodePlayers()
    {
        if (!AllNetcodePlayersSpawned)      // shouldn't be here yet if not! (event sequence)
            return;

        List<GamePlayer> allGamePlayers = TurnManager.Instance.AllGamePlayers;        // in index order
        List<NetcodePlayer> orderedNetcodePlayers = OrderedNetcodePlayers;        // in order NetcodePlayers spawned (ie. when they picked up join code)

        // attach each GamePlayer (and therefore PlayerTokens) to its corresponding NetcodePlayer
        // there may be fewer players than PlayerTokens...
        for (int tokensIndex = 0, playerIndex = 0; tokensIndex < TurnManager.Instance.AllGamePlayers.Count; tokensIndex++, playerIndex++)
        {
            if (playerIndex >= LobbyManager.Instance.NumberOfPlayers)      // cycle through players if fewer than PlayerTokens
                playerIndex = 0;

            NetcodePlayer netcodePlayer = orderedNetcodePlayers[playerIndex];
            GamePlayer gamePlayer = allGamePlayers[tokensIndex];

            netcodePlayer.LinkToGamePlayer(gamePlayer);       // 2-way link GamePlayer and NetcodePlayer
        }

        MultiplayerEvents.OnAllPlayersLinked?.Invoke();
    }

    public void RemoveNetcodePlayer(ulong ownerClientId)
    {
        NetcodePlayer player = netcodePlayers.FirstOrDefault(x => x.OwnerClientId == ownerClientId);

        if (player != null)
            netcodePlayers.Remove(player);
    }
}

