using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Services.Lobbies.Models;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// handles switching between lobby panels

public class LobbyUI : MonoBehaviour
{
    [SerializeField] private PlayerDataSO PlayerData;               // saved player data

    [SerializeField] private Image background;                  // semi-transparent black
    [SerializeField] private AuthenticateUI authenticateUI;     // input player name
    [SerializeField] private LobbyListUI lobbyListUI;           // list of available lobbies
    [SerializeField] private LobbyPlayersUI lobbyPlayersUI;     // players in joined lobby
    [SerializeField] private LobbyCreateUI lobbyCreateUI;       // input lobby name, visibility, max players amd game mode
    [SerializeField] private Image gamePanel;                   // name, credits, etc

    [SerializeField] private TextMeshProUGUI playerNameText;     
    [SerializeField] private TextMeshProUGUI debugText;

    private void OnEnable()
    {
        MultiplayerEvents.OnAuthenticated += OnAuthenticated;

        MultiplayerEvents.OnCreateLobby += OnCreateLobby;
        MultiplayerEvents.OnJoinedLobby += OnJoinedLobby;
        MultiplayerEvents.OnLeftLobby += OnLeftLobby;
        MultiplayerEvents.OnKickedFromLobby += OnKickedFromLobby;

        MultiplayerEvents.OnLobbyStartGame += OnLobbyStartGame;
    }

    private void OnDisable()
    {
        MultiplayerEvents.OnAuthenticated -= OnAuthenticated;

        MultiplayerEvents.OnCreateLobby -= OnCreateLobby;
        MultiplayerEvents.OnJoinedLobby -= OnJoinedLobby;
        MultiplayerEvents.OnLeftLobby -= OnLeftLobby;
        MultiplayerEvents.OnKickedFromLobby -= OnKickedFromLobby;

        MultiplayerEvents.OnLobbyStartGame -= OnLobbyStartGame;
    }

    private void Start()
    {
        background.gameObject.SetActive(true);
        gamePanel.gameObject.SetActive(true);

        lobbyPlayersUI.Hide();
        lobbyCreateUI.Hide();
        //authenticateUI.Hide();
        //lobbyListUI.Hide();

        if (!string.IsNullOrEmpty(PlayerData.PlayerName))
        {
            // no need to authenticate again (causes error!)
            playerNameText.text = PlayerData.PlayerName;
            authenticateUI.Hide();
            lobbyListUI.Show();
        }
        else
        {
            playerNameText.text = "";
            lobbyListUI.Hide();
            authenticateUI.Show();
        }

        playerNameText.gameObject.SetActive(true);
    }

    // show list of lobbies
    private void OnAuthenticated(string playerName, string playerId)
    {
        playerNameText.text = playerName;

        authenticateUI.Hide();
        lobbyListUI.Show();
        lobbyPlayersUI.Hide();
        lobbyCreateUI.Hide();
    }

    // show UI to set up new lobby
    private void OnCreateLobby()
    {
        authenticateUI.Hide();
        lobbyListUI.Hide();
        lobbyPlayersUI.Hide();
        lobbyCreateUI.Show();
    }

    // show list of lobbies
    private void OnLeftLobby()
    {
        authenticateUI.Hide();
        lobbyListUI.Show();
        lobbyPlayersUI.Hide();
        lobbyCreateUI.Hide();
    }

    // show list of lobbies
    private void OnKickedFromLobby()
    {
        authenticateUI.Hide();
        lobbyListUI.Show();
        lobbyPlayersUI.Hide();
        lobbyCreateUI.Hide();
    }

    // show lobby players
    private void OnJoinedLobby(Lobby joinedLobby, bool isLobbyHost, string playerName)
    {
        authenticateUI.Hide();
        lobbyListUI.Hide();
        //lobbyPlayersUI.Show();
        lobbyCreateUI.Hide();

        // start game immediately if only single player game
        if (LobbyManager.Instance.NumberOfPlayers == 1)
        {
            lobbyPlayersUI.Hide();
            LobbyManager.Instance.StartGame();              // create relay & start host
        }
        else
        {
            lobbyPlayersUI.Show();
        }
    }

    // hide everything when game started
    private void OnLobbyStartGame(LobbyPlayerData lobbyPlayerData)
    {
        authenticateUI.Hide();
        lobbyListUI.Hide();
        lobbyPlayersUI.Hide();
        lobbyCreateUI.Hide();

        background.gameObject.SetActive(false);
        gamePanel.gameObject.SetActive(false);
    }
}
