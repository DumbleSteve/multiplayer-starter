using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Services.Lobbies.Models;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;

// list of lobbies represented as buttons

public class LobbyListUI : MonoBehaviour
{
    [SerializeField] private Image lobbyListPanel;              // parent to all UI 

    [SerializeField] private LobbyListEntryUI lobbyButtonTemplate;  // acts as prefab for lobbiesContainer children
    [SerializeField] private Transform lobbiesContainer;            // vertical layout group
    [SerializeField] private Button refreshButton;
    [SerializeField] private Button createLobbyButton;


    private void Awake()
    {
        lobbyButtonTemplate.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        MultiplayerEvents.OnLobbyListChanged += OnLobbyListChanged;

        refreshButton.onClick.AddListener(RefreshButtonClick);
        createLobbyButton.onClick.AddListener(CreateLobbyButtonClick);
    }

    private void OnDisable()
    {
        MultiplayerEvents.OnLobbyListChanged -= OnLobbyListChanged;

        refreshButton.onClick.RemoveListener(RefreshButtonClick);
        createLobbyButton.onClick.RemoveListener(CreateLobbyButtonClick);
    }

    private void OnLobbyListChanged(List<Lobby> lobbyList)
    {
        // clear lobbies container, except for template!
        foreach (Transform child in lobbiesContainer)
        {
            LobbyListEntryUI childListEntryUI = child.GetComponent<LobbyListEntryUI>();
            if (childListEntryUI != null && childListEntryUI == lobbyButtonTemplate)
                continue;

            Destroy(child.gameObject);
        }

        // populate lobbies container
        foreach (Lobby lobby in lobbyList)
        {
            LobbyListEntryUI lobbyListEntryUI = Instantiate(lobbyButtonTemplate, lobbiesContainer);
            lobbyListEntryUI.gameObject.SetActive(true);
            lobbyListEntryUI.UpdateLobby(lobby);
        }
    }

    private void RefreshButtonClick()
    {
        LobbyManager.Instance.RefreshLobbyList();
        RefreshLobbiesPause();
    }

    // pause to prevent request limit being exceeded
    private async void RefreshLobbiesPause()
    {
        refreshButton.interactable = false;
        await Task.Delay((int)(LobbyManager.RefreshLobbyListInterval * 1000f));
        refreshButton.interactable = true;
    }

    private void CreateLobbyButtonClick()
    {
        MultiplayerEvents.OnCreateLobby?.Invoke();
        CreateLobbyPause();
    }

    // pause to prevent request limit being exceeded
    private async void CreateLobbyPause()
    {
        createLobbyButton.interactable = false;
        await Task.Delay((int)(LobbyManager.RefreshLobbyListInterval * 1000f));
        createLobbyButton.interactable = true;
    }

    public void Hide()
    {
        lobbyListPanel.gameObject.SetActive(false);
    }

    public void Show()
    {
        lobbyListPanel.gameObject.SetActive(true);
    }
}
