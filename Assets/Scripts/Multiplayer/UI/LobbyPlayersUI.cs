using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Services.Authentication;
using Unity.Services.Lobbies.Models;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;


// handles list of players in a single lobby

public class LobbyPlayersUI : MonoBehaviour
{
    [SerializeField] private Image lobbyPlayersPanel;              // parent to all UI

    [SerializeField] private LobbyPlayerEntryUI playerTemplate;     // acts as prefab for players vertical layout group
    [SerializeField] private Transform players;

    [SerializeField] private TextMeshProUGUI lobbyNameText;
    [SerializeField] private TextMeshProUGUI playerCountText;
    [SerializeField] private TextMeshProUGUI gameModeText;

    [SerializeField] private Button changeMarineButton;
    [SerializeField] private Button changeNinjaButton;
    [SerializeField] private Button changeZombieButton;
    [SerializeField] private Button changeGameModeButton;

    [SerializeField] private Button leaveLobbyButton;
    [SerializeField] private Button startGameButton;

    private float startGameDelay = 0.1f;         // pause before game starts

    private Pulser pulser;

    private bool isLobbyHost;
    private bool gameStarted = false;

    private void Awake()
    {
        playerTemplate.gameObject.SetActive(false);

        lobbyNameText.text = "";
        playerCountText.text = "";
        gameModeText.text = "";

        pulser = GetComponent<Pulser>();
    }

    private void OnEnable()
    {
        changeMarineButton.onClick.AddListener(MarineClicked);
        changeNinjaButton.onClick.AddListener(NinjaClicked);
        changeZombieButton.onClick.AddListener(ZombieClicked);

        changeGameModeButton.onClick.AddListener(ChangeGameMode);
        leaveLobbyButton.onClick.AddListener(LeaveLobby);
        startGameButton.onClick.AddListener(LobbyStartGame);

        MultiplayerEvents.OnJoinedLobby += OnJoinedLobby;
        MultiplayerEvents.OnLobbyUpdated += OnLobbyUpdated;
        MultiplayerEvents.OnLobbyGameModeChanged += OnLobbyUpdated;
        MultiplayerEvents.OnLeftLobby += OnLeftLobby;
        MultiplayerEvents.OnKickedFromLobby += OnLeftLobby;
    }

    private void OnDisable()
    {
        changeMarineButton.onClick.RemoveListener(MarineClicked);
        changeNinjaButton.onClick.RemoveListener(NinjaClicked);
        changeZombieButton.onClick.RemoveListener(ZombieClicked);

        changeGameModeButton.onClick.RemoveListener(ChangeGameMode);
        leaveLobbyButton.onClick.RemoveListener(LeaveLobby);
        startGameButton.onClick.RemoveListener(LobbyStartGame);

        MultiplayerEvents.OnJoinedLobby -= OnJoinedLobby;
        MultiplayerEvents.OnLobbyUpdated -= OnLobbyUpdated;
        MultiplayerEvents.OnLobbyGameModeChanged -= OnLobbyUpdated;
        MultiplayerEvents.OnLeftLobby -= OnLeftLobby;
        MultiplayerEvents.OnKickedFromLobby -= OnLeftLobby;
    }

    // short delay for visual effect
    private async void LobbyStartGame()
    {
        if (!isLobbyHost || gameStarted)
            return;

        gameStarted = true;                             // stop update on lobby poll
        startGameButton.interactable = false;           // to prevent spamming

        await Task.Delay((int)(startGameDelay * 1000f));
        LobbyManager.Instance.StartGame();              // create relay & start host
    }

    private void LeaveLobby()
    {
        leaveLobbyButton.interactable = false;          // to prevent spamming
        LobbyManager.Instance.LeaveLobby();
    }

    private void ChangeGameMode()
    {
        changeGameModeButton.interactable = false;      // to prevent spamming
        LobbyManager.Instance.ChangeLobbyGameMode();
    }

    private void ZombieClicked()
    {
        LobbyManager.Instance.ChangePlayerCharacter(LobbyManager.PlayerCharacter.Zombie);
    }

    private void NinjaClicked()
    {
        LobbyManager.Instance.ChangePlayerCharacter(LobbyManager.PlayerCharacter.Ninja);
    }

    private void MarineClicked()
    {
        LobbyManager.Instance.ChangePlayerCharacter(LobbyManager.PlayerCharacter.Marine);
    }

    private void OnLeftLobby()
    {
        ClearLobbyPlayers();
    }

    private void OnJoinedLobby(Lobby lobby, bool isLobbyHost, string playerName)
    {
        this.isLobbyHost = isLobbyHost;

        OnLobbyUpdated(lobby);
    }

    // lobby is polled regularly for updates
    // clear players from lobby UI and repopulate
    private void OnLobbyUpdated(Lobby lobby)
    {
        if (gameStarted)
            return;

        ClearLobbyPlayers();

        foreach (Player player in lobby.Players)
        {
            LobbyPlayerEntryUI lobbyPlayerEntryUI = Instantiate(playerTemplate, players);
            lobbyPlayerEntryUI.gameObject.SetActive(true);
            lobbyPlayerEntryUI.SetKickPlayerButtonVisible(isLobbyHost && player.Id != AuthenticationService.Instance.PlayerId);        // don't allow host kicking self!
            lobbyPlayerEntryUI.UpdatePlayer(player);
        }

        changeGameModeButton.gameObject.SetActive(isLobbyHost);
        changeGameModeButton.interactable = isLobbyHost;
         
        startGameButton.gameObject.SetActive(isLobbyHost);

        bool canStartGame = isLobbyHost && lobby.Players.Count == lobby.MaxPlayers;             // all players in lobby - game can start
        startGameButton.interactable = canStartGame; 

        lobbyNameText.text = lobby.Name;
        playerCountText.text = $"{lobby.Players.Count}/{lobby.MaxPlayers}";
        gameModeText.text = lobby.Data[LobbyManager.KEY_LOBBY_GAME_MODE].Value;
    }

    private void ClearLobbyPlayers()
    {
        foreach (Transform child in players)
        {
            LobbyPlayerEntryUI childPlayerEntry = child.GetComponent<LobbyPlayerEntryUI>();
            if (childPlayerEntry == playerTemplate)     // don't delete template!
                continue;

            Destroy(child.gameObject);
        }
    }

    public void Show()
    {
        lobbyPlayersPanel.gameObject.SetActive(true);

        startGameButton.interactable = false;
        leaveLobbyButton.interactable = true;
        changeGameModeButton.interactable = true;
    }

    public void Hide()
    {
        lobbyPlayersPanel.gameObject.SetActive(false);
    }
}
