//using System;
//using System.Collections;
//using System.Collections.Generic;
//using TMPro;
//using UnityEngine;
//using UnityEngine.UI;


//public class InputWindowUI : MonoBehaviour
//{
//    [SerializeField] private Image inputWindowPanel;
//    [SerializeField] private TextMeshProUGUI titleText;
//    [SerializeField] private TMP_InputField inputField;

//    [SerializeField] private Button okButton;
//    [SerializeField] private Button cancelButton;

//    private string inputValue;

//    private void OnEnable()
//    {
//        okButton.onClick.AddListener(OkClicked);
//        cancelButton.onClick.AddListener(CancelClicked);
//        inputField.onValueChanged.AddListener(InputValueChanged);
//    }

//    private void OnDisable()
//    {
//        okButton.onClick.RemoveListener(OkClicked);
//        cancelButton.onClick.RemoveListener(CancelClicked);
//        inputField.onValueChanged.RemoveListener(InputValueChanged);
//    }

//    private void InputValueChanged(string value)
//    {
//        inputValue = value;
//    }

//    private void OkClicked()
//    {
//        GameEvents.OnInputOkClicked?.Invoke(inputValue);
//    }

//    private void CancelClicked()
//    {
//        GameEvents.OnInputCancelClicked?.Invoke();
//    }

//    public void Show(string title)
//    {
//        titleText.text = title;
//        inputWindowPanel.gameObject.SetActive(true);
//    }

//    public void Hide()
//    {
//        inputWindowPanel.gameObject.SetActive(false);
//    }
//}
