using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Services.Lobbies.Models;
using UnityEngine;
using UnityEngine.UI;

// UI to represent a single player in LobbyPlayersUI (list of players in a lobby)

public class LobbyPlayerEntryUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI playerNameText;
    [SerializeField] private Image characterImage;
    [SerializeField] private Button kickPlayerButton;

    private Player player;


    private void OnEnable()
    {
        kickPlayerButton.onClick.AddListener(KickPlayer);
    }

    private void OnDisable()
    {
        kickPlayerButton.onClick.RemoveListener(KickPlayer);
    }

    public void SetKickPlayerButtonVisible(bool visible)
    {
        kickPlayerButton.gameObject.SetActive(visible);
    }

    public void UpdatePlayer(Player player)
    {
        this.player = player;

        playerNameText.text = LobbyManager.Instance.LobbyPlayerName(player);
        characterImage.sprite = LobbyManager.Instance.LobbyPlayerCharacter(player);
    }

    private void KickPlayer()
    {
        if (player != null)
        {
            kickPlayerButton.interactable = false;      // prevent spamming!
            LobbyManager.Instance.KickPlayer(player.Id);
        }
    }
}
