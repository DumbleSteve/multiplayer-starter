using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Services.Lobbies.Models;
using UnityEngine;
using UnityEngine.UI;

// button to represent a single lobby in LobbyListUI
// click to join player to selected lobby

public class LobbyListEntryUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI lobbyNameText;
    [SerializeField] private TextMeshProUGUI playersText;
    [SerializeField] private TextMeshProUGUI gameModeText;

    private Lobby lobby;
    private Button lobbyButton;

    private void Awake()
    {
        lobbyButton = GetComponent<Button>();
    }

    private void OnEnable()
    {
        lobbyButton.onClick.AddListener(LobbyButtonClicked);
    }

    private void OnDisable()
    {
        lobbyButton.onClick.RemoveListener(LobbyButtonClicked);
    }

    private void LobbyButtonClicked()
    {
        lobbyButton.interactable = false;           // to prevent spamming lobby server
        LobbyManager.Instance.JoinLobby(lobby);
    }

    public void UpdateLobby(Lobby lobby)
    {
        this.lobby = lobby;

        lobbyNameText.text = lobby.Name;
        playersText.text = $"{lobby.Players.Count}/{lobby.MaxPlayers}";
        gameModeText.text = lobby.Data[LobbyManager.KEY_LOBBY_GAME_MODE].Value;
    }
}
