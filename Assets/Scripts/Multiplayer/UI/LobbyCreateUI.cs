using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class LobbyCreateUI : MonoBehaviour
{
    [SerializeField] private Image lobbyCreatePanel;

    [SerializeField] private TMP_InputField lobbyNameInput;

    [SerializeField] private Button publicPrivateButton;
    [SerializeField] private Button maxPlayersButton;
    [SerializeField] private Button gameModeButton;

    [SerializeField] private Button createButton;

    [SerializeField] private TextMeshProUGUI publicPrivateText;
    [SerializeField] private TextMeshProUGUI maxPlayersText;
    [SerializeField] private TextMeshProUGUI gameModeText;

    [SerializeField] private TextMeshProUGUI errorText;      // game name problems
    //private float errorTime = 3f;

    private const int MaxNameLength = 20;

    private string lobbyName;
    private bool isPrivate;
    private int maxPlayers;
    private LobbyManager.GameMode gameMode;


    private void OnEnable()
    {
        createButton.onClick.AddListener(CreateLobbyClicked);
        publicPrivateButton.onClick.AddListener(PublicPrivateClicked);
        maxPlayersButton.onClick.AddListener(MaxPlayersClicked);
        gameModeButton.onClick.AddListener(GameModeClicked);

        lobbyNameInput.onValueChanged.AddListener(LobbyNameChanged);
    }

    private void OnDisable()
    {
        createButton.onClick.RemoveListener(CreateLobbyClicked);
        publicPrivateButton.onClick.RemoveListener(PublicPrivateClicked);
        maxPlayersButton.onClick.RemoveListener(MaxPlayersClicked);
        gameModeButton.onClick.RemoveListener(GameModeClicked);

        lobbyNameInput.onValueChanged.RemoveListener(LobbyNameChanged);
    }

    private void CreateLobbyClicked()
    {
        if (string.IsNullOrEmpty(lobbyName))
        {
            errorText.text = "Please enter a Name for your Game";
            return;
        }

        errorText.text = "";
        createButton.interactable = false;      // prevent spamming
        LobbyManager.Instance.CreateLobby(lobbyName, maxPlayers, isPrivate, gameMode);
    }

    private void PublicPrivateClicked()
    {
        isPrivate = !isPrivate;
        UpdateText();
    }

    private void MaxPlayersClicked()
    {
        maxPlayers++;

        if (maxPlayers > LobbyManager.PlayerLimit)
            maxPlayers = 1;

        UpdateText();
    }

    // cycle through modes
    private void GameModeClicked()
    {
        gameMode = LobbyManager.Instance.CycleGameMode(gameMode);
        UpdateText();
    }

    private void LobbyNameChanged(string newName)
    {
        errorText.text = "";

        // restrict length of name
        if (newName.Length > MaxNameLength)
            newName = lobbyNameInput.text = newName[..MaxNameLength];

        lobbyName = newName;
        createButton.interactable = lobbyName != "";
    }

    private void UpdateText()
    {
        lobbyNameInput.text = lobbyName;
        publicPrivateText.text = isPrivate ? "Private" : "Public";
        maxPlayersText.text = maxPlayers == 1 ? "1 Player" : $"{maxPlayers} Players";
        gameModeText.text = gameMode.ToString();
    }

    public void Hide()
    {
        lobbyCreatePanel.gameObject.SetActive(false);
    }

    public void Show()
    {
        lobbyCreatePanel.gameObject.SetActive(true);
        createButton.interactable = true;
        errorText.text = "";

        lobbyName = "";
        isPrivate = false;
        maxPlayers = LobbyManager.PlayerLimit;
        gameMode = LobbyManager.GameMode.GameMode1;

        UpdateText();
    }
}