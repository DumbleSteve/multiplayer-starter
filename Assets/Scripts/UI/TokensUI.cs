using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// list of buttons linked to PlayerTokens to (potentially)
// denote token/player health and current turn

[RequireComponent(typeof(ToggleGroup))]

public class TokensUI : MonoBehaviour
{
    [SerializeField] private TokensButton TokensButtonPrefab;

    private ToggleGroup toggleGroup;

    private void Awake()
    {
        toggleGroup = GetComponent<ToggleGroup>();       // required
    }

    private void OnEnable()
    {
        MultiplayerEvents.OnPlayerTokensSpawned += OnPlayerTokensSpawned;        // tokens button
    }

    private void OnDisable()
    {
        MultiplayerEvents.OnPlayerTokensSpawned -= OnPlayerTokensSpawned;
    }

    // create a new button for the tokens just spawned
    private void OnPlayerTokensSpawned(PlayerTokens tokens, Color playerColour)
    {
        TokensButton tokensButton = Instantiate(TokensButtonPrefab, transform);     // vertical layout group
        tokensButton.Init(tokens, toggleGroup, playerColour);
    }
}
