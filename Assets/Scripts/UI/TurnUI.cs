using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Threading.Tasks;
using System;


// UI for turn countdown / end turn button

public class TurnUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI timeText;                  // time remaining for turn
    [SerializeField] private Button endTurnButton;                      // 'skip turn' enabled OnIsLocalPlayersTurn - starts next player's turn

    [SerializeField] private TextMeshProUGUI yourTurnText;
    [SerializeField] private Pulser turnTextPulser;

    [SerializeField] private TextMeshProUGUI extraTurnsText;

    [SerializeField] private Button quitGameButton;                    // quit game      
    [SerializeField] private Button confirmQuitButton;                   // really quit?
    private const float confirmQuitTimeout = 3f;                        // time to confirm quit game

    [SerializeField] private Color turnInProgressColour = Color.white;
    [SerializeField] private Color timeWarningColour = Color.red;

    private GamePlayer currentTurnPlayer;
    private bool localPlayersTurn = false;

    private const float yourTurnPulseInterval = 2.5f;       // seconds
    private bool yourTurnPulsing = false;                   // don't start pulse again if same player's turn (eg. 1 player game or last PlayerTokens standing)

    private const float timeWarningPercent = 0.1f;                      // % of maxTime - timeText countdown changes to timeWarningColour
    private int timeWarningLevel => (int)(maxTime * timeWarningPercent);     // % of maxTime - timeText countdown changes to timeWarningColour

    private float maxTime;
    private float timeRemaining;                                        // counts down


    private void Awake()
    {
        confirmQuitButton.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        endTurnButton.onClick.AddListener(NextTurnClicked);
        quitGameButton.onClick.AddListener(QuitGameClicked);
        confirmQuitButton.onClick.AddListener(ConfirmQuitClicked);

        MultiplayerEvents.OnTurnTimeTick += OnTurnTimeTick;       
        MultiplayerEvents.OnIsLocalPlayersTurn += OnIsLocalPlayersTurn;
        MultiplayerEvents.OnExtraTurns += OnExtraTurns;
        MultiplayerEvents.OnTokenPlayed += OnTokenPlayed;
    }

    private void OnDisable()
    {
        endTurnButton.onClick.RemoveListener(NextTurnClicked);
        quitGameButton.onClick.RemoveListener(QuitGameClicked);
        confirmQuitButton.onClick.RemoveListener(ConfirmQuitClicked);

        MultiplayerEvents.OnTurnTimeTick -= OnTurnTimeTick;
        MultiplayerEvents.OnIsLocalPlayersTurn -= OnIsLocalPlayersTurn;
        MultiplayerEvents.OnExtraTurns -= OnExtraTurns;
        MultiplayerEvents.OnTokenPlayed -= OnTokenPlayed;
    }

    private void Start()
    {
        timeText.text = $"---";
        yourTurnText.text = "";
        extraTurnsText.text = "";
    }

    private void OnTurnTimeTick(int maxTime, int timeRemaining, float turnTickInterval)
    {
        this.maxTime = maxTime;
        this.timeRemaining = timeRemaining;

        timeText.text = $"{(this.timeRemaining > 0 ? this.timeRemaining : "---")}";
        timeText.color = (this.timeRemaining > 0 && this.timeRemaining <= timeWarningLevel) ? timeWarningColour : turnInProgressColour;
    }

    private void OnIsLocalPlayersTurn(GamePlayer currentTurnPlayer, bool isLocalPlayersTurn, int numPlayers)
    {
        this.currentTurnPlayer = currentTurnPlayer;
        localPlayersTurn = isLocalPlayersTurn;

        quitGameButton.gameObject.SetActive(true);
        confirmQuitButton.gameObject.SetActive(false);

        endTurnButton.interactable = isLocalPlayersTurn;
        quitGameButton.interactable = isLocalPlayersTurn;
        confirmQuitButton.interactable = isLocalPlayersTurn;

        yourTurnText.text = isLocalPlayersTurn && numPlayers > 1 ? "Your Turn!" : "";
        yourTurnText.color = currentTurnPlayer.PlayerColour;

        if (isLocalPlayersTurn && numPlayers > 1 && !yourTurnPulsing)        // don't start coroutine again if same player's turn (eg. 1 player game or last PlayerTokens standing)
            StartCoroutine(PulseYourTurn());
    }

    private IEnumerator PulseYourTurn()
    {
        if (turnTextPulser == null || yourTurnPulsing)
            yield break;

        yourTurnPulsing = true;

        while (localPlayersTurn)
        {
            turnTextPulser.Pulse(yourTurnText.transform);
            yield return new WaitForSeconds(yourTurnPulseInterval);
        }

        yourTurnPulsing = false;
    }

    private void OnExtraTurns(int extraTurns)
    {
        extraTurnsText.text = $"Extra Turns: {extraTurns}";

        if (extraTurns > 0)
            turnTextPulser.Pulse(extraTurnsText.transform);
    }

    private void OnTokenPlayed(PlayerToken playedToken)
    {
        endTurnButton.interactable = false;
        quitGameButton.interactable = false;
        confirmQuitButton.interactable = false;
    }

    private void NextTurnClicked()
    {
        if (!NetworkInterface.Instance.IsLocalPlayersTurn)            // to make sure!
            return;

        endTurnButton.interactable = false;
        quitGameButton.interactable = false;
        confirmQuitButton.interactable = false;

        MultiplayerEvents.OnTurnStart?.Invoke(false);      // set next player and start turn for all players
    }

    private async void QuitGameClicked()
    {
        if (!NetworkInterface.Instance.IsLocalPlayersTurn)            // to make sure!
            return;

        quitGameButton.interactable = false;
        quitGameButton.gameObject.SetActive(false);
        confirmQuitButton.gameObject.SetActive(true);

        await Task.Delay((int)(confirmQuitTimeout * 1000f));

        if (! currentTurnPlayer.PlayerOutOfGame)      // confirm not clicked, restore quit button
        {
            confirmQuitButton.gameObject.SetActive(false);
            quitGameButton.gameObject.SetActive(true);
            quitGameButton.interactable = true;
        }
    }

    private void ConfirmQuitClicked()
    {
        if (!NetworkInterface.Instance.IsLocalPlayersTurn)            // to make sure!
            return;

        confirmQuitButton.gameObject.SetActive(false);
        quitGameButton.gameObject.SetActive(true);
        quitGameButton.interactable = true;

        NetworkInterface.Instance.SyncPlayerOut(currentTurnPlayer.NetcodePlayerId, TurnManager.Instance.CurrentPlayerTurnIndex, false);
    }
}
