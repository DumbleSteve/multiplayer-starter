using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;


// handles all game status text,
// the 'play again' and music mute buttons

public class UIController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI CurrentPlayer;
    [SerializeField] private TextMeshProUGUI StatusText;

    [SerializeField] private Image GameOverPanel;
    [SerializeField] private Button PlayAgainButton;       // reload scene

    [SerializeField] private TurnUI TurnUI;                // enabled OnLobbyStartGame
    [SerializeField] private MusicMuteUI MusicMuteUI;      // enabled OnLobbyStartGame

    [SerializeField] private TextMeshProUGUI debugText;

    private float timedOutStatusTime = 3f;                  // seconds

    private string currentPlayerName;
    private Color currentPlayerColour;

    private Pulser pulser;                                  // may be null!

    private void Awake()
    {
        pulser = GetComponent<Pulser>();

        CurrentPlayer.text = "";
        StatusText.text = "";

        ShowAll(false);         // shown on game started (via lobby)

        TurnUI.gameObject.SetActive(false);
        MusicMuteUI.gameObject.SetActive(false);
        GameOverPanel.gameObject.SetActive(false);
    }

    private void Start()
    {
        LeanTween.init(3200);
    }

    private void OnEnable()
    {
        PlayAgainButton.onClick.AddListener(PlayAgainClicked);

        MultiplayerEvents.OnLobbyStartGame += OnLobbyStartGame;                      // via lobby
        MultiplayerEvents.OnAllTokensSpawned += OnAllTokensSpawned;   

        MultiplayerEvents.OnTurnStart += OnTurnStart;
        MultiplayerEvents.OnTurnChanged += OnTurnChanged;
        MultiplayerEvents.OnGameOver += OnGameOver;                      // all players 'out'
    }

    private void OnDisable()
    {
        PlayAgainButton.onClick.RemoveListener(PlayAgainClicked);

        MultiplayerEvents.OnLobbyStartGame -= OnLobbyStartGame;
        MultiplayerEvents.OnAllTokensSpawned -= OnAllTokensSpawned;

        MultiplayerEvents.OnTurnStart -= OnTurnStart;
        MultiplayerEvents.OnTurnChanged -= OnTurnChanged;
        MultiplayerEvents.OnGameOver -= OnGameOver;

        //MultiplayerEvents.OnIsLocalPlayersTurn -= OnIsLocalPlayersTurn;
    }

    private void OnLobbyStartGame(LobbyPlayerData lobbyPlayerData)
    {
        ShowAll(true);

        //if (LobbyManager.Instance.NumberOfPlayers > 1)
        //    StatusText.text = "Connecting Players...";
        //else
            StatusText.text = "Setting Up...";
    }

    private void OnTurnChanged(GamePlayer currentPlayer)
    {
        currentPlayerName = currentPlayer.PlayerName;
        currentPlayerColour = currentPlayer.PlayerColour;

        CurrentPlayer.text = currentPlayer != null ? currentPlayerName : "";
        CurrentPlayer.color = currentPlayerColour;

        if (pulser != null)
            pulser.Pulse(CurrentPlayer.transform);

        StatusText.text = "";
    }

    private void OnAllTokensSpawned(List<PlayerTokens> allPlayerTokens)
    {
        TurnUI.gameObject.SetActive(true);
        MusicMuteUI.gameObject.SetActive(true);
    }

    private async void OnTurnStart(bool timedOut)
    {
        if (timedOut)
        {
            StatusText.text = "Time's Up!";
            if (pulser != null)
                pulser.Pulse(StatusText.transform);

            await Task.Delay((int)(timedOutStatusTime * 1000f));
            StatusText.text = "";
        }
    }

    private void OnGameOver(GamePlayer winner)
    {
        TurnUI.gameObject.SetActive(false);
        MusicMuteUI.gameObject.SetActive(false);
        GameOverPanel.gameObject.SetActive(true);

        StatusText.color = winner.PlayerColour;
        StatusText.text = $"{winner.PlayerName} Wins!!";

        if (pulser != null)
            pulser.Pulse(StatusText.transform);
    }

    private void ShowAll(bool show)
    {
        GameOverPanel.gameObject.SetActive(false);  // only show on game over!
        StatusText.text = "";
    }

    private void PlayAgainClicked()
    {
        MultiplayerEvents.OnPlayAgain?.Invoke();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);     // stays signed in to relay, so no need to authenticate again (PlayerName saved in SO)
    }
}
