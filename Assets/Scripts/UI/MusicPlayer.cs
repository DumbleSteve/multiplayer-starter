using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class MusicPlayer : MonoBehaviour
{
    [SerializeField] private PlayerDataSO PlayerData;       // saved player data

    [SerializeField] private AudioClip MusicAudio;

    private AudioSource musicSource;

    private void Start()
    {
        musicSource = GetComponent<AudioSource>();
        musicSource.clip = MusicAudio;

        MultiplayerEvents.OnMusicMuted?.Invoke(PlayerData.MusicMuted);
    }

    private void OnEnable()
    {
        MultiplayerEvents.OnLobbyStartGame += OnLobbyStartGame;
        MultiplayerEvents.OnToggleMusicMute += OnToggleMusicMute;
    }

    private void OnDisable()
    {
        MultiplayerEvents.OnLobbyStartGame -= OnLobbyStartGame;
        MultiplayerEvents.OnToggleMusicMute -= OnToggleMusicMute;
    }


    private void OnLobbyStartGame(LobbyPlayerData lobbyPlayerData)
    {
        if (musicSource.clip != null && !PlayerData.MusicMuted)
            musicSource.Play();
    }

    private void OnToggleMusicMute()
    {
        if (musicSource.clip == null)
            return;

        PlayerData.MusicMuted = !PlayerData.MusicMuted;

        if (PlayerData.MusicMuted)
            musicSource.Pause();
        else
            musicSource.Play();

        MultiplayerEvents.OnMusicMuted?.Invoke(PlayerData.MusicMuted);
    }
}
