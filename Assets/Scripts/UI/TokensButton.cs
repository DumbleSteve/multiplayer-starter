using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


// toggle button representing a PlayerTokens object (owned by a GamePlayer)

[RequireComponent(typeof(Toggle))]

public class TokensButton : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI playerName;
    [SerializeField] private TextMeshProUGUI playerOut;
    [SerializeField] private TextMeshProUGUI winner;
    [SerializeField] private TextMeshProUGUI rank;

    [SerializeField] private TextMeshProUGUI tokensName;
    [SerializeField] private HorizontalLayoutGroup tokensPanel;
    [SerializeField] private Image tokenImagePrefab;
    private List<Image> tokenImages = new();

    [SerializeField] private Image currentPlayerFlag;
    [SerializeField] private Image characterSprite;                 // sprite from lobby

    [SerializeField] private Color goldColour = Color.yellow;       // rank
    [SerializeField] private Color silverColour = Color.cyan;       // rank
    [SerializeField] private Color bronzeColour = Color.red;        // rank
    [SerializeField] private Color unrankedColour = Color.grey;     // rank

    private Color onColour;                                         // background
    private Color offColour;                                        // background
    private const float transparency = 0.25f;                       // off / out of game

    private Color spriteDisabledColor = new(1, 1, 1, transparency);
    private float spritePulseFactor = 1.4f;
    private float spritePulseTime = 0.5f;
    private Vector3 spriteScale;

    private float tokenHealthFadeTime = 0.5f;

    private Toggle button;
    private PlayerTokens buttonTokens = null;           // PlayerTokens (therefore GamePlayer) that this button is linked to
    private int PlayerRanking => buttonTokens.GamePlayer.Data.Ranking;

    private bool gameOver;

    private Image background;
    private Pulser pulser;


    private void Awake()
    {
        button = GetComponent<Toggle>();
        background = GetComponent<Image>();
        pulser = GetComponent<Pulser>();

        spriteScale = characterSprite.transform.localScale;

        SetCurrentPlayer(false);

        playerOut.gameObject.SetActive(false);
        winner.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        button.onValueChanged.AddListener(SelectTokens);                       // camera orbits PlayerTokens      // TODO: reinstate manual tokens selection?

        MultiplayerEvents.OnAllTokensSpawned += OnAllTokensSpawned;            // first PlayerTokens is selected once all spawned
        MultiplayerEvents.OnAllPlayersLinked += OnAllPlayersLinked;            // all NetCodePlayers and GamePlayers linked up

        MultiplayerEvents.OnTurnChanged += OnTurnChanged;                      // on turn start

        MultiplayerEvents.OnTokenHealthChanged += OnTokenHealthChanged;
        MultiplayerEvents.OnPlayerOutOfGame += OnPlayerOutOfGame;               
        MultiplayerEvents.OnGameOver += OnGameOver;                            // all PlayerTokens 'out'
    }

    private void OnDisable()
    {
        button.onValueChanged.RemoveListener(SelectTokens);

        MultiplayerEvents.OnAllTokensSpawned -= OnAllTokensSpawned;
        MultiplayerEvents.OnAllPlayersLinked -= OnAllPlayersLinked;

        MultiplayerEvents.OnTurnChanged -= OnTurnChanged;

        MultiplayerEvents.OnTokenHealthChanged -= OnTokenHealthChanged;
        MultiplayerEvents.OnPlayerOutOfGame -= OnPlayerOutOfGame;
        MultiplayerEvents.OnGameOver -= OnGameOver;
    }

    private void OnAllTokensSpawned(List<PlayerTokens> allPlayerTokens)
    {
        button.interactable = true;
    }

    public void Init(PlayerTokens tokens, ToggleGroup group, Color playerColour)
    {
        buttonTokens = tokens;

        tokenImages.Clear();

        // UI image for each token
        for (int i = 0; i < tokens.Tokens.Count; i++)
        {
            Image tokenImage = Instantiate(tokenImagePrefab, tokensPanel.transform);      // auto-sized by vertical layout group

            tokenImage.color = playerColour;
            tokenImages.Add(tokenImage);
        }

        tokensName.text = name = tokens.TokensName;
        //tokensName.color = playerColour;
        playerName.text = tokens.PlayerName;
        //playerName.color = playerColour;

        onColour = playerColour;
        offColour = new Color(playerColour.r, playerColour.g, playerColour.b, transparency);        // semi-transparent
        SetBackgroundColour();

        button.group = group;               // toggle group
        button.interactable = false;        // until all PlayerTokens built

        rank.text = "";

        if (pulser != null)
            pulser.Pulse(transform);
    }

    // set player character sprite
    private void OnAllPlayersLinked()
    {
        characterSprite.sprite = buttonTokens.GamePlayer.PlayerCharacter;    // LobbyPlayerData set when linked to NetcodePlayer
    }

    // when button toggled on (player clicked or set on change of turn)
    private void SelectTokens(bool selected)
    {
        if (gameOver)
            return;

        SetBackgroundColour();

        if (selected)
            MultiplayerEvents.OnPlayerTokensSelected?.Invoke(buttonTokens);
    }

    private void OnTurnChanged(GamePlayer newTurnPlayer)
    {
         SetCurrentPlayer(buttonTokens == newTurnPlayer.PlayerTokens);
    }

    private void SetCurrentPlayer(bool enable)
    {
        currentPlayerFlag.enabled = enable;
        button.isOn = enable;       // -> SelectTokens()

        SetBackgroundColour();

        if (enable && pulser != null)
            pulser.Pulse(transform);

        characterSprite.color = enable ? Color.white : spriteDisabledColor;

        LeanTween.scale(characterSprite.gameObject, enable ? spriteScale * spritePulseFactor : spriteScale, spritePulseTime)
                            .setEaseOutBack();
    }

    private void SetBackgroundColour()
    {
        if (button.isOn)
            background.color = onColour;
        else
            background.color = offColour;
    }

    private void OnTokenHealthChanged(PlayerToken token, int tokenIndex, float maxHealth)
    {
        if (token.ParentTokens != buttonTokens) 
            return;

        Image tokenImage = tokenImages[tokenIndex];
        SetTokenHealthColour(tokenImage, token.Health, maxHealth);
    }

    // colour fades as health fades...
    private void SetTokenHealthColour(Image tokenImage, float health, float maxHealth)
    {
        if (maxHealth <= 0)
            return;

        float healthPercent = health / maxHealth;
        Color healthColour = new(tokenImage.color.r, tokenImage.color.g, tokenImage.color.b, healthPercent);

        LeanTween.value(tokenImage.gameObject, tokenImage.color, healthColour, tokenHealthFadeTime)
                    .setOnUpdate((Color c) => tokenImage.color = c)
                    .setEaseOutQuad();
    }
     
    private void OnPlayerOutOfGame(GamePlayer player, bool isWinner)
    {
        if (buttonTokens.GamePlayer != player)
            return;

        //Debug.Log($"<color=yellow>OnPlayerOutOfGame player {player.PlayerTokens.TokensName}  isWinner {isWinner}</color>");

        switch (PlayerRanking)
        {
            case 4:
                rank.color = unrankedColour;
                rank.text = "4th";
                break;

            case 3:
                rank.color = bronzeColour;
                rank.text = "3rd";
                break;

            case 2:
                rank.color = silverColour;
                rank.text = "2nd";
                break;

            case 1:
                rank.color = goldColour;
                rank.text = "1st";
                break;

            default:
                rank.color = unrankedColour;
                rank.text = PlayerRanking.ToString();
                break;
        }

        SetCurrentPlayer(false);

        button.isOn = isWinner;
        SetBackgroundColour();

        playerOut.gameObject.SetActive(!isWinner);
        winner.gameObject.SetActive(isWinner);
        characterSprite.gameObject.SetActive(false);

        if (pulser != null)
            pulser.Pulse(rank.transform);
    }

    private void OnGameOver(GamePlayer winner)
    {
        gameOver = true;
    }
}
