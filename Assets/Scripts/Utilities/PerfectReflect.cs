﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

// reflect (token) at exactly the collision velocity (unless below minVelocity) and angle
public class PerfectReflect : MonoBehaviour
{
    [SerializeField] private LayerMask reflectLayers; 
    [SerializeField] private float minVelocity = 3f;     // minimum reflection velocity (eg. pinball boost)
    [SerializeField] private bool doReflect = true;      // so can be easily turned off

    private Vector3 lastFrameVelocity;
    private Rigidbody rb;


    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        // save velocity so it can be maintained on reflection
        if (doReflect)
            lastFrameVelocity = rb.velocity;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (doReflect)
            Reflect(collision);
    }

    private void Reflect(Collision collision)
    {
        int hitLayerMask = 1 << collision.gameObject.layer;
        bool hitReflectLayer = ((reflectLayers & hitLayerMask) != 0);       // true if hitLayerMask is one of the reflectLayers

        //Debug.Log($"Reflect: {name} from {collision.transform.name} hitReflectLayer {hitReflectLayer} lastFrameVelocity {lastFrameVelocity}");

        if (!hitReflectLayer)       // not a perfect reflection layer, so allow physics to take its natural course
            return;

        // force reflection velocity to lastFrameVelocity.magnitude
        Vector3 collisionNormal = collision.contacts[0].normal;     // perpendicular to reflect surface
        Vector3 reflectDirection = Vector3.Reflect(lastFrameVelocity.normalized, collisionNormal);  // in direction, in normal
        rb.velocity = reflectDirection * Mathf.Max(lastFrameVelocity.magnitude, minVelocity);
    }
}
