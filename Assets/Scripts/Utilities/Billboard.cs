using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    [SerializeField] private bool lockX;
    [SerializeField] private bool lockY;
    [SerializeField] private bool lockZ;

    private Transform mainCam;
    private Vector3 startRotation;

    private void Awake()
    {
        startRotation = transform.rotation.eulerAngles;
        mainCam = Camera.main.transform;
    }

    private void LateUpdate()
    {
        transform.forward = mainCam.forward;

        // lock rotation around x/y/z axes if required 
        Vector3 rotation = transform.rotation.eulerAngles;

        if (lockX) rotation.x = startRotation.x;
        if (lockY) rotation.y = startRotation.y;
        if (lockZ) rotation.z = startRotation.z;

        transform.rotation = Quaternion.Euler(rotation);
    }
}
