using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Services.Lobbies.Models;

public static class MultiplayerEvents
{
    // cinemachine camera

    public delegate void OnPlayerTokensSelectedDelegate(PlayerTokens playerTokens);
    public static OnPlayerTokensSelectedDelegate OnPlayerTokensSelected;      // look at centre of PlayerTokens

    // token raycasting

    public delegate void OnTokenRaycastHitDelegate(List<PlayerToken> hitTokens);
    public static OnTokenRaycastHitDelegate OnTokenRaycastHit;

    public delegate void OnTokenHitDepthChangedDelegate(int hitDepth);          // >1 ray hits multiple tokens
    public static OnTokenHitDepthChangedDelegate OnTokenHitDepthChanged;

    public delegate void OnTokenRaycastEndDelegate(List<PlayerToken> hitTokens);
    public static OnTokenRaycastEndDelegate OnTokenRaycastEnd;

    // token 'moves'
    public delegate void OnTokenPlayedDelegate(PlayerToken playedToken);
    public static OnTokenPlayedDelegate OnTokenPlayed;

    // players

    public delegate void OnPlayerTokensSpawnedDelegate(PlayerTokens tokens, Color tokensColour); 
    public static OnPlayerTokensSpawnedDelegate OnPlayerTokensSpawned;

    public delegate void OnAllPlayersLinkedDelegate();
    public static OnAllPlayersLinkedDelegate OnAllPlayersLinked;    // all NetCodePlayers and GamePlayers linked up

    public delegate void OnAllTokensSpawnedDelegate(List<PlayerTokens> allPlayerTokens);
    public static OnAllTokensSpawnedDelegate OnAllTokensSpawned;     // after all players linked

    public delegate void OnTokenHealthChangedDelegate(PlayerToken token, int tokenIndex, float maxHealth);
    public static OnTokenHealthChangedDelegate OnTokenHealthChanged;   

    public delegate void OnPlayerOutOfGameDelegate(GamePlayer playerOut, bool isWinner);
    public static OnPlayerOutOfGameDelegate OnPlayerOutOfGame;      // player quit game or eg. zero health, tokens, credit etc.

    // turns

    public delegate void OnTurnStartDelegate(bool timedOut);
    public static OnTurnStartDelegate OnTurnStart;        // either 'end turn' clicked or timed out

    public delegate void OnTurnTimeTickDelegate(int maxTime, int timeRemaining, float turnTickInterval);
    public static OnTurnTimeTickDelegate OnTurnTimeTick;

    public delegate void OnTurnChangedDelegate(GamePlayer newTurnPlayer);   // immediately after start of each turn - after next player selected
    public static OnTurnChangedDelegate OnTurnChanged;

    public delegate void OnExtraTurnsDelegate(int extraTurns);              // when extra turns increased, used or reset (on new turn)
    public static OnExtraTurnsDelegate OnExtraTurns;

    public delegate void OnIsLocalPlayersTurnDelegate(GamePlayer currentTurnPlayer, bool isLocalPlayersTurn, int numPlayers);    // fired via ClientRPC by NetcodePlayer
    public static OnIsLocalPlayersTurnDelegate OnIsLocalPlayersTurn;

    public delegate void OnPlayAgainDelegate();
    public static OnPlayAgainDelegate OnPlayAgain;

    // game state

    public delegate void OnGameReadyToPlayDelegate();
    public static OnGameReadyToPlayDelegate OnGameReadyToPlay;              // game setup (board, tokens, etc) complete

    public delegate void OnGameOverDelegate(GamePlayer winner);
    public static OnGameOverDelegate OnGameOver;                            // 1 player remains

    // multiplayer lobby / relay

    public delegate void OnAuthenticatedDelegate(string playerName, string playerId);        // call back after authentication
    public static OnAuthenticatedDelegate OnAuthenticated;

    public delegate void OnAuthenticateErrorDelegate(string error);                    // eg. illegal player name
    public static OnAuthenticateErrorDelegate OnAuthenticateError;

    public delegate void OnCreateLobbyDelegate();
    public static OnCreateLobbyDelegate OnCreateLobby;              // host -> UI form to fill out

    public delegate void OnJoinedLobbyDelegate(Lobby joinedLobby, bool isLobbyHost, string playerName);
    public static OnJoinedLobbyDelegate OnJoinedLobby;              // host and client

    public delegate void OnLobbyUpdatedDelegate(Lobby joinedLobby);
    public static OnLobbyUpdatedDelegate OnLobbyUpdated;              // as a result of polling

    public delegate void OnLobbyListChangedDelegate(List<Lobby> lobbies);
    public static OnLobbyListChangedDelegate OnLobbyListChanged;

    public delegate void OnLobbyGameModeChangedDelegate(Lobby lobby);
    public static OnLobbyGameModeChangedDelegate OnLobbyGameModeChanged;

    public delegate void OnLeftLobbyDelegate();
    public static OnLeftLobbyDelegate OnLeftLobby;                  // host and client

    public delegate void OnKickedFromLobbyDelegate();
    public static OnKickedFromLobbyDelegate OnKickedFromLobby;

    public delegate void OnLobbyStartDelegate(LobbyPlayerData lobbyPlayerData);
    public static OnLobbyStartDelegate OnLobbyStartGame;             // by host from lobby -> on relay created (host) / joined (client) -> hides lobby UI etc

    // music

    public delegate void OnToggleMusicMuteDelegate();
    public static OnToggleMusicMuteDelegate OnToggleMusicMute;      // via UI button -> MusicPlayer

    public delegate void OnMusicMutedDelegate(bool muted);
    public static OnMusicMutedDelegate OnMusicMuted;                // via MusicPlayer -> UI button
}
