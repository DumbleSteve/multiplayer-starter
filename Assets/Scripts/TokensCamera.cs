using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System;


// enables Cinemachine camera orbiting of a specific PlayerTokens
// only while left mouse is held down

public class TokensCamera : MonoBehaviour
{
    [SerializeField] private CinemachineFreeLook OrbitCamera;       // set in inspector

    private string XAxisName = "Mouse X";
    private string YAxisName = "Mouse Y";

    private bool allTokensSpawned = false;        // start reading mouse input on all spawned

    private PlayerTokens playerTokens;

    // use high priority to transition to (make live) another camera
    private readonly int standbyPriority = 10;
    private readonly int livePriory = 100;

    private bool CameraIsLive => OrbitCamera.Priority == livePriory;


    private void Awake()
    {
        OrbitCamera.Priority = standbyPriority;

        // disable orbiting (until left mouse is held down)
        OrbitCamera.m_XAxis.m_InputAxisName = "";
        OrbitCamera.m_YAxis.m_InputAxisName = "";
    }

    private void OnEnable()
    {
        MultiplayerEvents.OnAllTokensSpawned += OnAllTokensSpawned;                // enables free look mouse control
        MultiplayerEvents.OnPlayerTokensSelected += OnPlayerTokensSelected;        // look at tokens
    }

    private void OnDisable()
    {
        MultiplayerEvents.OnAllTokensSpawned -= OnAllTokensSpawned;
        MultiplayerEvents.OnPlayerTokensSelected -= OnPlayerTokensSelected;
    }

    // only enable orbiting when left mouse button is held down
    private void Update()
    {
        if (!allTokensSpawned)
            return;

        if (Input.GetMouseButton(0))        // left mouse button
        {
            OrbitCamera.m_XAxis.m_InputAxisValue = Input.GetAxis(XAxisName);
            OrbitCamera.m_YAxis.m_InputAxisValue = Input.GetAxis(YAxisName);
        }
        else
        {
            OrbitCamera.m_XAxis.m_InputAxisValue = 0;
            OrbitCamera.m_YAxis.m_InputAxisValue = 0;
        }
    }

    // link PlayerTokens for camera LookAt/Follow
    public void SetPlayerTokens(PlayerTokens tokens)
    {
        playerTokens = tokens;
        LookAtPlayerTokens();
    }

    // selected by change of turn or player clicking tokens button
    // if this camera is for the selected PlayerTokens, set camera to 'live' priority to transition to it
    // else set to 'standby' priority and look at player's tokens
    private void OnPlayerTokensSelected(PlayerTokens tokens)
    {
        if (tokens == null)
            return;

        if (tokens == playerTokens)          // PlayerTokens at this PlayerPosition were selected
        {
            OrbitCamera.Priority = livePriory;          // becomes live camera
        }
        else
        {
            OrbitCamera.Priority = standbyPriority;     // camera on standby
        }

        LookAtPlayerTokens();
    }


    private void LookAtPlayerTokens()
    {
        OrbitCamera.LookAt = playerTokens.transform;
        OrbitCamera.Follow = playerTokens.transform;
    }

    private void OnAllTokensSpawned(List<PlayerTokens> tokens)
    {
        allTokensSpawned = true;      // start reading mouse input
    }
}
