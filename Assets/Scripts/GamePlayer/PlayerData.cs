
// data for each player (max time etc)

[System.Serializable]
public class PlayerData
{
    public int PlayerIndex;

    public LobbyPlayerData LobbyPlayerData;         // from lobby player

    public int MaxTime = 240;         // 'time' counts down every tickIntervalSeconds, resets at start of turn
    public bool OutOfGame = false;     
    public int Ranking = 0;
}
