using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Player Data", fileName = "Player Data")]

public class PlayerDataSO : ScriptableObject
{
    public string PlayerName = "";          // reset on LobbyManager.Awake()

    public int PlayCount = 0;               // TODO: persistent
    public int SessionPlayCount = 0;        // TODO: current play session (via lobby once)

    public bool HintsHidden = false;        // stay hidden once player hides them (button) - player be re-show any time
    public bool MusicMuted = false;        // stay hidden once player hides them (button) - player be re-show any time
}
