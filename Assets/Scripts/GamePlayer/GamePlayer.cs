using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Instantiated from prefab by TurnManager - one for each colour.
// Always one GamePlayer per PlayerTokens and vice versa.
//
// Potentially multiple GamePlayers per NetcodePlayer,
// as number of GamePlayers is dictated by TurnManager number of colours

public class GamePlayer : MonoBehaviour
{
    [SerializeField] public PlayerTokens PlayerTokens;      // tokens owned by this player
    [SerializeField] public PlayerData Data;                // visible in inspector

    public Color PlayerColour { get; private set; }         // defined in TurnManager
    public Material TokenMaterial { get; private set; }         // defined in TurnManager

    public string PlayerName => Data.LobbyPlayerData.PlayerName; 
    public Sprite PlayerCharacter => Data.LobbyPlayerData.PlayerCharacter;

    // each NetcodePlayer will control one or more GamePlayers
    public NetcodePlayer NetcodePlayer { get; private set; }
    public ulong NetcodePlayerId => NetcodePlayer.OwnerClientId;

    public bool PlayerOutOfGame => Data.OutOfGame;


    private void OnEnable()
    {
        MultiplayerEvents.OnPlayAgain += OnPlayAgain;
    }

    private void OnDisable()
    {
        MultiplayerEvents.OnPlayAgain -= OnPlayAgain;
    }

    // assign player and token colours and initial token position
    public void Init(int playerIndex, Color playerColour, Material tokenColour, Vector3 tokenPosition)
    {
        Data.PlayerIndex = playerIndex;

        PlayerColour = playerColour;        // UI etc.
        transform.position = tokenPosition;

        TokenMaterial = tokenColour;
    }

    // link this GamePlayer to its (single) NetcodePlayer
    // and set player name & character from lobby.
    // called when all NetcodePlayers have been spawned
    public void LinkNetcodePlayer(NetcodePlayer netcodePlayer)
    {
        NetcodePlayer = netcodePlayer;
        Data.LobbyPlayerData = netcodePlayer.LobbyPlayerData;

        // append lobby player name (for editor)
        name += $" ({PlayerName})";
    }

    public void SetPlayerOutOfGame(bool isWinner)
    {
        if (Data.OutOfGame)
            return;

        Data.OutOfGame = true;
        Data.Ranking = TurnManager.Instance.PlayersLeftInGame + 1;

        MultiplayerEvents.OnPlayerOutOfGame?.Invoke(this, isWinner);
    }

    private void OnPlayAgain()
    {
        Destroy(gameObject);
    }
}
