using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows;


// represents a token, piece, character, etc. that a GamePlayer controls / owns

[RequireComponent(typeof(Renderer))]         // for hilight
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]        // for raycast hit

public class PlayerToken : MonoBehaviour
{
    [SerializeField] private Transform Token;
    [SerializeField] protected float MaxHealth = 100f;

    public Vector3 RaycastOrigin { get; private set; }
    public Vector3 RaycastHitPoint { get; private set; }
    public Vector3 RaycastHitNormal { get; private set; }

    public float Health { get; protected set; } = 0f;
    public bool OutOfGame { get; protected set; } = false;

    //public virtual bool CanMove { get; protected set; } = true;
    public virtual bool CanMove => IsCurrentTurnPlayer && !InPlay && !OutOfGame && !TurnManager.Instance.TurnPlayingOut;

    public virtual bool InPlay { get; protected set; } = false;

    public PlayerTokens ParentTokens { get; private set; }
    private int TokenIndex => ParentTokens.Tokens.IndexOf(this);

    public bool IsCurrentTurnPlayer => ParentTokens.GamePlayer == TurnManager.Instance.CurrentTurnPlayer;
    public string PlayerName => ParentTokens.PlayerName;

    private Color tokenColour;
    private Renderer tokenRenderer;
    protected Rigidbody tokenRigidbody;


    // TODO: currently these must be in same order as TurnManager player colours
    public enum TokenType
    {
        Cyan = 0,
        Yellow = 1,
        Magenta = 2,
        Green = 3
    }

    public TokenType TokenSet { get; private set; }

    private void Awake()
    {
        tokenRigidbody = Token.GetComponent<Rigidbody>();
        tokenRenderer = Token.GetComponent<Renderer>();

        RaycastHitPoint = transform.position;       // centre by default
        RaycastOrigin = transform.position;       // centre by default
    }

    protected virtual void OnEnable()
    {
        MultiplayerEvents.OnTokenRaycastHit += OnTokenRaycastHit;
        MultiplayerEvents.OnTokenRaycastEnd += OnTokenRaycastEnd;
    }

    protected virtual void OnDisable()
    {
        MultiplayerEvents.OnTokenRaycastHit -= OnTokenRaycastHit;
        MultiplayerEvents.OnTokenRaycastEnd -= OnTokenRaycastEnd;
    }

    private void OnTokenRaycastHit(List<PlayerToken> tokens)
    {
        bool raycastHitToken = tokens != null && tokens.Count > 0 && tokens[0] == this;

        TokenSelected(raycastHitToken);            // virtual
    }

    private void OnTokenRaycastEnd(List<PlayerToken> tokens)
    {
        if (tokens == null || tokens.Count == 0 || tokens[0] != this)
            return;

        Move();     // virtual
    }

    public void Init(Vector3 position, Material tokenMaterial, PlayerTokens parentTokens)
    {
        transform.position = position;

        ParentTokens = parentTokens;

        Health = MaxHealth;
        OutOfGame = false;

        tokenColour = tokenMaterial.color;

        if (Token.TryGetComponent<Renderer>(out var renderer))
            renderer.material = tokenMaterial;
    }

    protected virtual void TokenSelected(bool raycastHitToken)
    {
        // overrides to perform action (on raycast hit)
    }

    protected virtual void Move()
    {
        // overrides to perform action (on raycast and right mouse up)
    }

    protected void Hilight(Color colour)
    {
        tokenRenderer.material.color = colour;
    }

    protected void ResetHilight()
    {
        Hilight(tokenColour);
    }

    public void SetRaycastHitPoint(Vector3 hitPoint, Vector3 hitNormal, Vector3 raycastOrigin)
    {
        RaycastHitPoint = hitPoint;
        RaycastHitNormal = hitNormal;
        RaycastOrigin = raycastOrigin;
    }


    public void TakeDamage(float damage)
    {
        Health -= damage;

        if (Health < 0)
        {
            Health = 0;
            OutOfGame = true;
        }

        if (Health > MaxHealth)
            Health = MaxHealth;

        MultiplayerEvents.OnTokenHealthChanged?.Invoke(this, TokenIndex, MaxHealth);
    }
}
