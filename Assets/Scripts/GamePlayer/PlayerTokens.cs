using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;


// set of PlayerToken objects that are owned by a GamePlayer

public class PlayerTokens : MonoBehaviour
{
    [SerializeField] public GamePlayer GamePlayer;             // player that owns these tokens (set in inspector)
    [SerializeField] public Light TurnLight;                    // lit on player+tokens turn

    private float lightIntensity;
    private float highIntensityFactor = 1.5f;

    public string PlayerName => GamePlayer.PlayerName;
    public string TokensName => $"Player {GamePlayer.Data.PlayerIndex+1} Tokens";

    [SerializeField] private PlayerToken tokenPrefab;
    //private float tokenSpawnDelay = 0.1f;                 // spawn interval
    private float tokenSpawnY = 1f;                         // when spawned (dropped)
    private float tokenCircleRadius = 0.75f;

    public List<PlayerToken> Tokens { get; private set; } = new();

    private void Awake()
    {
        lightIntensity = TurnLight.intensity;
        TurnLight.enabled = false;
    }

    private void OnEnable()
    {
        MultiplayerEvents.OnTurnChanged += OnTurnChanged;       // turn tokens light on/off
    }

    private void OnDisable()
    {
        MultiplayerEvents.OnTurnChanged -= OnTurnChanged;
    }

    public void SpawnPlayerTokens(int tokensPerPlayer, Material tokenMaterial, Color playerColour)
    {
        InstantiateInCircle(transform.position, tokenMaterial, tokensPerPlayer, tokenCircleRadius, tokenSpawnY);

        TurnLight.color = playerColour;
        TurnLight.enabled = true;

        MultiplayerEvents.OnPlayerTokensSpawned?.Invoke(this, playerColour);
    }

    private void InstantiateInCircle(Vector3 location, Material tokenMaterial, int tokensPerPlayer, float radius, float yPosition)
    {
        float angleSection = Mathf.PI * 2f / tokensPerPlayer;       // angle of each token 'pie slice'

        for (int i = 0; i < tokensPerPlayer; i++)
        {
            float tokenAngle = i * angleSection;
            Vector3 newPos = location + new Vector3(Mathf.Cos(tokenAngle), yPosition, Mathf.Sin(tokenAngle)) * radius;
            PlayerToken newToken = Instantiate(tokenPrefab, transform);
            newToken.Init(newPos, tokenMaterial, this);

            Tokens.Add(newToken);
        }
    }

    private void OnTurnChanged(GamePlayer newTurnPlayer) 
    {
        //TurnLight.enabled = GamePlayer == newTurnPlayer;
        TurnLight.intensity = GamePlayer == newTurnPlayer ? lightIntensity * highIntensityFactor : lightIntensity;
    }
}
