using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading.Tasks;

// raycasts for tokens

public class TokenRaycaster : MonoBehaviour
{
    [SerializeField] private LayerMask tokenLayers;     // for raycast hit

    [SerializeField] private AudioClip damageTokenDepthAudio;     // when damageTokenDepth changes

    // raycasting
    private Ray mouseRay;                               // ray from mouse screen position
    private float rayCastDistance = 15f;                // raycast 'reach'
    private float mouseWheelDeltaThreshold = 0.5f;      // to increase/decrease raycast 'depth' (number of tokens)

    private float rayCastInterval = 0.05f;              // (seconds) so not every frame...
    private bool raycasting = false;                    // every rayCastInterval seconds

    // raycast hits
    private const int hitArraySize = maxDamageTokenDepth * 2;    // double size of hit array as hits are not in proximity order
    private RaycastHit[] rayCastHits = new RaycastHit[hitArraySize];

    private readonly List<PlayerToken> hitTokens = new();    // from raycast, ordered by proximity - tokens to activate
    //private readonly List<Vector3> hitPoints = new();       // from raycast

    // raycast token depth
    private const int maxDamageTokenDepth = 5;              // max number of tokens 'deep' into hitTokens list to damage
    private const int minDamageTokenDepth = 1;              // min number of tokens 'deep' into hitTokens list to damage
    private int damageTokenDepth = 1;                       // current number of tokens 'deep' into hitTokens list to damage - set via mouse wheel


    private Camera mainCam;


    private void OnEnable()
    {

    }

    private void OnDisable()
    {

    }

    private void Start()
    {
        // get Camera.main once only for efficiency
        mainCam = Camera.main;
        damageTokenDepth = 1;
        MultiplayerEvents.OnTokenHitDepthChanged?.Invoke(damageTokenDepth);     // initial value, for UI
    }

    // detect mouse inputs for raycasting
    private void Update()
    {
        if (!NetworkInterface.Instance.IsLocalPlayersTurn)
            return;

        // right-mouse to raycast
        if (Input.GetMouseButtonDown(1))                    
        {
            StartRaycasting();
        }

        if (Input.GetMouseButtonUp(1))                      // stop raycasting
        {
            raycasting = false;
            MultiplayerEvents.OnTokenRaycastEnd?.Invoke(hitTokens);
            //Debug.Log($"OnTokenRaycastEnd: hitTokens.Count {hitTokens.Count}");
        }
    }

    // loop until raycasting is false
    private async void StartRaycasting()
    {
        if (raycasting || !NetworkInterface.Instance.IsLocalPlayersTurn)
            return;

        raycasting = true;

        while (raycasting && NetworkInterface.Instance.IsLocalPlayersTurn)
        {
            if (DoRaycast(Input.mousePosition))                     // tokens raycast hits
            {
                if (hitTokens.Count > 0)
                    GetMouseWheelDelta();                             // token hit depth
            }
 
            await Task.Delay((int)(rayCastInterval * 1000f));
        }
    }

    // raycast for target collider layer from mouse/touch screenPosition
    // make a list of tokens hit, according to damageTokenDepth
    private bool DoRaycast(Vector2 mousePosition)
    {
        mouseRay = mainCam.ScreenPointToRay(mousePosition);
        hitTokens.Clear();
        //hitPoints.Clear();

        int tokenHitCount = 0;
        Array.Clear(rayCastHits, 0, hitArraySize);

        // raycast to detect hit tokens
        tokenHitCount = Physics.RaycastNonAlloc(mouseRay, rayCastHits, rayCastDistance, tokenLayers);

        if (tokenHitCount > 0)
        {
            int numTokensToHit = Mathf.Min(damageTokenDepth, tokenHitCount);

            // sort all hits in order of ray hit distance
            List<RaycastHit> orderedByProximity = rayCastHits.OrderBy(hit => hit.distance).ToList();

            // make a list of PlayerTokens to take damage
            foreach (RaycastHit hit in orderedByProximity)
            {
                if (hit.transform == null)      // may be 'empty' hit (from array.clear)
                    continue;

                if (!hit.transform.TryGetComponent<PlayerToken>(out var hitToken))
                    continue;

                hitToken.SetRaycastHitPoint(hit.point, hit.normal, mainCam.transform.position);
                hitTokens.Add(hitToken);
                //hitPoints.Add(hit.point);

                if (hitTokens.Count == numTokensToHit)
                    break;
            }
        }

        MultiplayerEvents.OnTokenRaycastHit?.Invoke(hitTokens.Count > 0 ? hitTokens : null);   // hilight tokens
        //Debug.Log($"OnRaycastHit: hitTokens.Count {hitTokens.Count}");

        return tokenHitCount > 0;
    }

    // while raycasting tokens, detect mouse wheel scroll to change damageTokenDepth
    private void GetMouseWheelDelta()
    {
        if (raycasting && hitTokens.Count > 0)          // hitting tokens!
        {
            float mouseWheelDelta = Input.mouseScrollDelta.y;

            //if (mouseWheelDelta != 0)
            //    Debug.Log($"GetMouseWheelDelta: mouseWheelDelta {mouseWheelDelta}  mouseWheelThreshold {mouseWheelDeltaThreshold}");

            if (Mathf.Abs(mouseWheelDelta) > mouseWheelDeltaThreshold)
            {
                var currentTokenDepth = damageTokenDepth;       // to detect a change in value

                if (mouseWheelDelta > mouseWheelDeltaThreshold)          // mouse wheel up
                {
                    damageTokenDepth--;
                }
                else if (mouseWheelDelta < mouseWheelDeltaThreshold)
                {
                    damageTokenDepth++;
                }

                damageTokenDepth = Mathf.Clamp(damageTokenDepth, minDamageTokenDepth, maxDamageTokenDepth);

                if (damageTokenDepth != currentTokenDepth)
                {
                    MultiplayerEvents.OnTokenHitDepthChanged?.Invoke(damageTokenDepth);

                    if (damageTokenDepthAudio != null)
                        AudioSource.PlayClipAtPoint(damageTokenDepthAudio, transform.position);
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (!Input.GetMouseButton(1))
            return;

        Gizmos.color = Color.red;

        var rayStart = mouseRay.GetPoint(0);
        var rayEnd = rayStart + (mouseRay.direction * rayCastDistance);

        Gizmos.DrawLine(rayStart, rayEnd);
    }
}