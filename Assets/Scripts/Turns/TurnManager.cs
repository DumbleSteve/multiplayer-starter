using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading.Tasks;
using Unity.Netcode;

// handles turn countdown and switching GamePlayer turns
//
// manages GamePlayers' turns in conjunction with NetworkTurnManager singleton

public class TurnManager : MonoBehaviour
{
    public static TurnManager Instance { get; private set; }       // singleton

    [SerializeField] private GamePlayer playerPrefab;

    [SerializeField] private List<Color> playerColours = new();             // one GamePlayer per colour
    [SerializeField] private List<Material> tokenColours = new();           // for PlayerTokens
    [SerializeField] private List<TokensCamera> playerPositions = new();     // for PlayerTokens / virtual cameras

    [SerializeField] private int tokensPerPlayer;                           // for PlayerTokens
    public List<PlayerTokens> AllPlayerTokens { get; private set; } = new();
    public List<PlayerToken> TokensInPlay { get; private set; } = new();    // until all tokens stopped moving          

    [SerializeField] private AudioClip gameOverAudio;                       // all players 'out'

    public List<GamePlayer> AllGamePlayers { get; private set; } = new();
    public GamePlayer CurrentTurnPlayer { get; private set; } = null;
    public int CurrentPlayerTurnIndex { get; private set; } = -1;
    public bool TurnPlayingOut { get; private set; } = false;               // can't move whole tokens 'in play'

    private int extraTurns = 0;

    public int PlayerCount => AllGamePlayers.Count;
    public GamePlayer GamePlayerByIndex(int playerIndex) => AllGamePlayers.FirstOrDefault(x => x.Data.PlayerIndex == playerIndex);
    public int PlayersLeftInGame => AllGamePlayers.Count(x => !x.Data.OutOfGame);

    // counts down every tickIntervalSeconds, resets at start of turn
    // safety net: use first player if currentTurnPlayer not yet set (it's a timing / network thing)
    private int CurrentPlayerMaxTime => (CurrentTurnPlayer != null) ? CurrentTurnPlayer.Data.MaxTime : AllGamePlayers[0].Data.MaxTime;

    private float turnTickInterval = 0.15f;                 // time between turnTimeRemaining decrements
    private float tickTimeRemaining;                        // time until next tick to decrement turn or playOut countdown

    private int turnTimeRemaining = 0;                      // waiting for player to make their move
    private bool turnCountdownRunning = false;

    private bool isLocalPlayersTurn;


    private void Awake()
    {
        Instance = this;        // singleton
    }

    private void OnEnable()
    {
        MultiplayerEvents.OnLobbyStartGame += OnLobbyStartGame;
        MultiplayerEvents.OnAllPlayersLinked += OnAllPlayersLinked;         // spawn tokens
        MultiplayerEvents.OnTurnStart += OnTurnStart;                       // end turn clicked or timed out
        MultiplayerEvents.OnTurnChanged += OnTurnChanged;                  // start of turn after current player set
        MultiplayerEvents.OnIsLocalPlayersTurn += OnIsLocalPlayersTurn;
        MultiplayerEvents.OnPlayAgain += OnPlayAgain;
        MultiplayerEvents.OnGameOver += OnGameOver;
    }

    private void OnDisable()
    {
        MultiplayerEvents.OnLobbyStartGame -= OnLobbyStartGame;
        MultiplayerEvents.OnAllPlayersLinked -= OnAllPlayersLinked;
        MultiplayerEvents.OnTurnStart -= OnTurnStart;
        MultiplayerEvents.OnTurnChanged -= OnTurnChanged;
        MultiplayerEvents.OnIsLocalPlayersTurn -= OnIsLocalPlayersTurn;
        MultiplayerEvents.OnPlayAgain -= OnPlayAgain;
        MultiplayerEvents.OnGameOver -= OnGameOver;
    }


    // countdown timer
    private void Update()
    {
        if (!turnCountdownRunning)
            return;

        tickTimeRemaining -= Time.deltaTime;

        // decrement time remaining by 1
        if (tickTimeRemaining < 0)
        {
            turnTimeRemaining--;

            MultiplayerEvents.OnTurnTimeTick?.Invoke(CurrentPlayerMaxTime, turnTimeRemaining, turnTickInterval);

            tickTimeRemaining = turnTickInterval;        // reset tick timer

            // start new turn if timeRemaining reaches zero
            if (turnTimeRemaining <= 0)
            {
                turnTimeRemaining = 0;
                turnCountdownRunning = false;

                if (isLocalPlayersTurn)
                    MultiplayerEvents.OnTurnStart?.Invoke(true);
            }
        }
    }

    private void OnLobbyStartGame(LobbyPlayerData lobbyPlayerData)
    {
        turnCountdownRunning = false;
        TokensInPlay.Clear();

        SpawnGamePlayers();
    }

    // don't spawn PlayerTokens until GamePlayers have been linked
    // to their NetcodePlayer (PlayerName)
    private void SpawnGamePlayers()
    {
        for (int i = 0; i < playerColours.Count; i++)
        {
            GamePlayer newPlayer = Instantiate(playerPrefab, transform);

            AllGamePlayers.Add(newPlayer);
            newPlayer.name = $"Player {PlayerCount}";     // gameobject name - not displayed in game

            // assign colour, position and index to new GamePlayer
            int playerIndex = PlayerCount - 1;
            var tokensCamera = playerPositions[playerIndex];
            newPlayer.Init(playerIndex, playerColours[playerIndex], tokenColours[playerIndex], tokensCamera.transform.position);

            tokensCamera.SetPlayerTokens(newPlayer.PlayerTokens);
        }
    }

    private void OnAllPlayersLinked()
    {
        SpawnAllPlayerTokens();   
    }

    private void SpawnAllPlayerTokens()
    {
        AllPlayerTokens.Clear();

        foreach (GamePlayer player in AllGamePlayers)
        {
            PlayerTokens playerTokens = player.PlayerTokens;
            AllPlayerTokens.Add(playerTokens);

            playerTokens.SpawnPlayerTokens(tokensPerPlayer, player.TokenMaterial, player.PlayerColour);
        }

        // all player tokens spawned
        MultiplayerEvents.OnAllTokensSpawned?.Invoke(AllPlayerTokens);
    }

    // new turn player set - start turn timer
    private void OnTurnChanged(GamePlayer newTurnPlayer)
    {
        tickTimeRemaining = turnTickInterval;

        turnTimeRemaining = CurrentPlayerMaxTime;
        turnCountdownRunning = true;

        CurrentTurnPlayer = newTurnPlayer;
        CurrentPlayerTurnIndex = newTurnPlayer.Data.PlayerIndex;

        MultiplayerEvents.OnTurnTimeTick?.Invoke(CurrentPlayerMaxTime, turnTimeRemaining, turnTickInterval);

        extraTurns = 0;
        MultiplayerEvents.OnExtraTurns?.Invoke(extraTurns);
    }

    private void OnIsLocalPlayersTurn(GamePlayer currentTurnPlayer, bool isLocalPlayersTurn, int numPlayers)
    {
        this.isLocalPlayersTurn = isLocalPlayersTurn;
    }

    // relays currentTurnPlayer to all netcode players
    // to update turn 'status' for all players
    private void OnTurnStart(bool timedOut)
    {
        if (AllGamePlayers == null || PlayerCount == 0)
        {
            Debug.LogError("OnTurnStart: no Players!!");
            return;
        }

        GamePlayer winner = GetGameWinner();        // null if game not over yet
        if (winner != null)           // all players but one have no max charge or out of game
        {
            turnCountdownRunning = false;

            if (gameOverAudio != null)
                AudioSource.PlayClipAtPoint(gameOverAudio, transform.position);

            NetworkInterface.Instance.SyncPlayerOut(winner.NetcodePlayerId, winner.Data.PlayerIndex, true);
            return;
        }

        if (extraTurns > 0)         // don't increment CurrentPlayerTurnIndex
        {
            extraTurns--;
            MultiplayerEvents.OnExtraTurns?.Invoke(extraTurns);
        }
        else if (CurrentTurnPlayer == null || CurrentPlayerTurnIndex == -1)     // increment to next GamePlayer
        {
            CurrentPlayerTurnIndex = 0;
        } 
        else
        {
            CurrentPlayerTurnIndex++;
            if (CurrentPlayerTurnIndex >= PlayerCount)       // loop round to first player again
                CurrentPlayerTurnIndex = 0;

            //Debug.Log($"<color=white> TurnManager.OnNextPlayerTurn currentPlayerIndex {currentPlayerTurnIndex} currentTurnPlayer {currentTurnPlayer.PlayerName} MaxTime {currentTurnPlayer.Data.MaxTime}</color>");
        }

        CurrentTurnPlayer = AllGamePlayers[CurrentPlayerTurnIndex];

        // skip player if has no time credit left or is out of game
        if (CurrentTurnPlayer.Data.MaxTime <= 0 || CurrentTurnPlayer.PlayerOutOfGame)
        {
            OnTurnStart(false);       // recursive
            return;
        }

        // update 'turn status' and start turn for all players
        NetworkInterface.Instance.SyncPlayerTurns(CurrentTurnPlayer.NetcodePlayerId, CurrentTurnPlayer.Data.PlayerIndex);
    }

    // a token was played by a player
    // stop turn countdown
    public void TokenPlayed(PlayerToken playedToken)
    {
        if (!TurnPlayingOut)
            TurnPlayingOut = true;

        turnCountdownRunning = false;

        TokensInPlay.Add(playedToken);
        //Debug.Log($"<color=yellow>TokenPlayed TokensInPlay.Count {TokensInPlay.Count}</color>");
    }

    // a token in play has played out (eg. stopped moving)
    public void TokenPlayedOut(PlayerToken playedToken)
    {
        if (!TokensInPlay.Contains(playedToken))
            return;
 
        TokensInPlay.Remove(playedToken);
        //Debug.Log($"<color=lime>TokenPlayedOut TokensInPlay.Count {TokensInPlay.Count}</color>");

        // start new turn when last playedToken stops moving
        if (isLocalPlayersTurn && TokensInPlay.Count == 0)
        {
            TurnPlayingOut = false;
            MultiplayerEvents.OnTurnStart?.Invoke(false);
        }
    }

    public void AddExtraTurn()
    {
        extraTurns++;
        MultiplayerEvents.OnExtraTurns?.Invoke(extraTurns);
    }

    public GamePlayer GetGameWinner()
    {
        if (PlayersLeftInGame > 1)
            return null;

        if (PlayerCount == 1 && PlayersLeftInGame == 1)
            return null;

        // 1 player left (or zero if 1 player game)
        return AllGamePlayers.FirstOrDefault(x => !x.Data.OutOfGame);
    }

    private void OnGameOver(GamePlayer winner)
    {
        TokensInPlay.Clear();
    }

    private void OnPlayAgain()
    {
        NetworkManager.Singleton.Shutdown();
        AllPlayerTokens.Clear();
        AllGamePlayers.Clear();
    }
}
